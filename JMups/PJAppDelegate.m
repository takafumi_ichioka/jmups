//
//  AppDelegate.m
//  JMups
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJAppDelegate.h"
#import "PJProgressView.h"
#import "PJNavigationViewController.h"
#import "PJMenuInitCertificationPadViewController.h"
#import "PJLoginPadViewController.h"
#import "PJMenuTabBarPadViewController.h"
#import "NSString+JMups.h"
#import "PJMenuInformationUtils.h"

#define kInitCertificationInformationKeyName     @"InitCertificationInformation"

@interface PJAppDelegate ()

@property(nonatomic) BOOL executeActivate;
@property(nonatomic,strong) PJProgressView* progressView;

@end

@implementation PJAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    PJLibInterface* interface = [PJLibInterface sharedInterface];
    interface.delegate = self;
    [interface settingURLInfoPlistName:@"JMupsURL"];
    PJBasePadViewController* padController;
    if (interface.keychainInformation.activateInformation) {
        padController = [[PJLoginPadViewController alloc] initWithNibName:nil bundle:nil];
        self.executeActivate = [NSString isNullOrEmptyString:interface.keychainInformation.uniqueId];
    } else {
        self.executeActivate = NO;
        NSDictionary* menuInformation = [PJMenuInformationUtils menuInformationForKey:kInitCertificationInformationKeyName];
        NSArray* menus = [PJMenuInformationUtils menusForMenuInformation:menuInformation];
        NSMutableArray* menuList = @[].mutableCopy;
        for (int i = 0; i < menus.count; i++) {
            PJMenuInformation* menuInformation = [PJMenuInformationUtils generateMenuInfomationWithMenu:menus[i] index:i];
            [menuList addObject:menuInformation];
        }
        padController = [[PJMenuInitCertificationPadViewController alloc] initWithMenuList:menuList
                                                                                     title:[PJMenuInformationUtils menuTextForMenuInformation:menuInformation]];
    }
    
    PJNavigationViewController* naviController = [[PJNavigationViewController alloc] initWithRootViewController:padController];
    self.window.rootViewController = naviController;
//    self.window.rootViewController = [[PJMenuTabBarPadViewController alloc] initWithNibName:nil bundle:nil];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if (self.executeActivate) {
        self.executeActivate = NO;
        self.progressView = [[PJProgressView alloc] initWithSuperView:self.window.rootViewController.view
                                                              message:NSLocalizedString(@"activite progress message", @"start activate")
                                                         progressType:PJProgressViewTypeMulti];
        __block PJAppDelegate* blockSelf = self;
        [self.progressView startWithStopHandler:^{
            [blockSelf.progressView removeFromSuperview];
            PJLog(@"complete");
        }];
        NSString* path = [[NSBundle mainBundle] pathForResource:@"ActivateInformation" ofType:@"plist"];
        NSDictionary* activateList = [NSDictionary dictionaryWithContentsOfFile:path];
        PJActivateInformation* information = [[PJActivateInformation alloc] init];
        information.subscribeSequence = activateList[@"SubscribeSequence"];
        information.activateId = activateList[@"ActivateId"];
        information.oneTimePassword = activateList[@"OneTimePassword"];
        [PJLibInterface sharedInterface].activeInformation = information;
        [[PJLibInterface sharedInterface] startActivate];
        
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) updateActivateProgress:(float)progress{
    self.progressView.barProgress = progress;
}

-(void) didCompletedActivate{
    PJLog(@"didCompletedActivate");
    [self.progressView stop];
}
     
-(void) didFaildActivateErrorMessage:(NSString *)errorMessage{
    PJLog(@"didCompletedActivate = %@",errorMessage);
    [self.progressView stop];
}

@end
