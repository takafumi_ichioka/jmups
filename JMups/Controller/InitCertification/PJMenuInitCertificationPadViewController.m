//
//  PJMenuInitCertificationPadViewController.m
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJMenuInitCertificationPadViewController.h"
#import "PJInitCertificationView.h"
#import "PJLicenseCertificationView.h"
#import "PJMenuTabBarPadViewController.h"



@interface PJMenuInitCertificationPadViewController ()<PJInitCertificationViewDelegate,PJLicenseCertificationViewDelegate>

@property(nonatomic,strong) PJLicenseCertificationView* licenseView;
@property(nonatomic,strong) PJInitCertificationView* certificationView;

@end

@implementation PJMenuInitCertificationPadViewController

-(void) loadView{
    [super loadView];
    int index = 0;
    for (PJMenuInformation* menuInfo in self.menuList) {
        switch (index) {
            case 0:
                self.licenseView = [[PJLicenseCertificationView alloc] initWithController:self title:menuInfo.text];
                self.licenseView.alpha = 0;
                self.licenseView.delegate = self;
                
                break;
            case 1:
                self.certificationView =  [[PJInitCertificationView alloc] initWithController:self title:menuInfo.text];
                self.certificationView.alpha = 0;
                self.certificationView.delegate = self;
                break;
            default:
                break;
        }
        index += 1;
    }
    [self.view sendSubviewToBack:self.licenseView];
    [self.view sendSubviewToBack:self.certificationView];
}

-(void) didSelectedMenuInformation:(PJMenuInformation *)information{
    CGRect menuViewFrame = self.menuViewFrame;
    UIView* openView;
    switch (information.index) {
        case 0:
            openView = self.licenseView;
            break;
        case 1:
            openView = self.certificationView;
            break;
        default:
            break;
    }
    CGRect openViewFrame = openView.frame;
    openViewFrame.origin.x = CGRectGetMaxX(menuViewFrame) - kMenuViewDiff;
    [UIView animateWithDuration:0.25 animations:^{
        openView.frame = openViewFrame;
        openView.alpha = 1.0;
    }];
}

-(void) didExecuteAuth:(PJInitCertificationView *)view userInfo:(NSDictionary *)userInfo{
    PJMenuTabBarPadViewController* menuTabController = [[PJMenuTabBarPadViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:menuTabController animated:YES];
}

-(void) didExecuteAuth:(PJLicenseCertificationView *)view licenseKey:(NSString *)licenseKey{
    CGRect licenseViewFrame = self.licenseView.frame;
    licenseViewFrame.origin.x = 0;
    __weak PJMenuInitCertificationPadViewController* weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.licenseView.frame = licenseViewFrame;
        weakSelf.licenseView.alpha = 0;
    } completion:^(BOOL finished){
        [weakSelf selectedMenuInformation:weakSelf.menuList[1]];
        CGRect certificationViewFrame = weakSelf.certificationView.frame;
        certificationViewFrame.origin.x = CGRectGetMaxX(weakSelf.menuViewFrame) - kMenuViewDiff;
        [UIView animateWithDuration:0.25 animations:^{
            weakSelf.certificationView.frame = certificationViewFrame;
            weakSelf.certificationView.alpha = 1.0;
        }];
    }];
}

@end
