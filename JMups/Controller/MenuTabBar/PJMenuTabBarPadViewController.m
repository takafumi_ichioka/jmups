//
//  PJMenuTabBarPadViewController.m
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJMenuTabBarPadViewController.h"
#import "PJMenuBisinessPadViewController.h"
#import "PJMenuSumPadViewController.h"
#import "PJMenuReprintViewController.h"
#import "PJMenuSettingPadViewController.h"
#import "PJMenuOtherPadViewController.h"
#import "PJInformationPadViewController.h"
#import "PJMenuInformationUtils.h"

#define kMenuInformationTabsKeyName     @"MenuInformationTabs"
#define kInformationTabKeyName          @"InformationTab"
#define kBusinessKeyName                @"Business"
#define kSumKeyName                     @"Sum"
#define kReprintKeyName                 @"Reprint"
#define kSettingsKeyName                @"Settings"
#define kOtherKeyName                   @"Other"

#define kTabBarItemFontSize 14

@interface PJMenuTabBarPadViewController ()

@end

@implementation PJMenuTabBarPadViewController

-(void) viewDidLoad{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

-(void) loadView{
    [super loadView];
    self.tabBar.itemPositioning = UITabBarItemPositioningFill;
    self.tabBar.selectedImageTintColor = [UIColor colorWithRed:0.2 green:0.2 blue:1.0 alpha:1.0];
    self.tabBar.barTintColor = [UIColor colorWithRed:0.9 green:1.0 blue:1.0 alpha:0.9];
    [UITabBarItem appearance].titlePositionAdjustment = UIOffsetMake(0, -2);
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:kTabBarItemFontSize],
                                                        NSForegroundColorAttributeName:[UIColor blackColor]}
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:kTabBarItemFontSize],
                                                        NSForegroundColorAttributeName:[UIColor colorWithRed:0.2 green:0.2 blue:1.0 alpha:1.0]}
                                             forState:UIControlStateSelected];
    [self settingChildViewControllers];
}


-(void) settingChildViewControllers{
    NSDictionary* informationTab = [PJMenuInformationUtils menuInformationForKey:kInformationTabKeyName];
    NSArray* menuTabList = [self infomationListWithMenuInfomation:[PJMenuInformationUtils menuInformationForKey:kMenuInformationTabsKeyName]];
    int menuTabIndex = 0;
    for (NSDictionary* menuTab  in menuTabList) {
        PJMenuBasePadViewController* menuPadViewController;
        NSString* menuText = [PJMenuInformationUtils menuTextForMenuInformation:menuTab];
        NSArray*  menus = [PJMenuInformationUtils menusForMenuInformation:menuTab];
        NSMutableArray* menuList = @[].mutableCopy;
        if (menus) {
            for (NSDictionary* menu in menus) {
                PJMenuInformation* menuInfomation = [PJMenuInformationUtils generateMenuInfomationWithMenu:menu
                                                                                                     index:menuTabIndex];
                [menuList addObject:menuInfomation];
            }
        }
        switch (menuTabIndex) {
            case 0:
                menuPadViewController = [[PJMenuBisinessPadViewController alloc] initWithMenuList:menuList title:menuText];
                break;
            case 1:
                menuPadViewController = [[PJMenuSumPadViewController alloc] initWithMenuList:menuList title:menuText];
                break;
            case 2:
                menuPadViewController = [[PJMenuReprintViewController alloc] initWithMenuList:menuList title:menuText];
                break;
            case 3:
                menuPadViewController = [[PJMenuSettingPadViewController alloc] initWithMenuList:menuList title:menuText];
                break;
            case 4:
                menuPadViewController = [[PJMenuOtherPadViewController alloc] initWithMenuList:menuList title:menuText];
                break;
            default:
                break;
        }
        if (menuPadViewController) {
            [self addChildViewController:menuPadViewController];
            menuPadViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:menuText
                                                                             image:nil
                                                                               tag:menuTabIndex];
        }
        menuTabIndex += 1;
    }
    
    PJInformationPadViewController* informationController = [[PJInformationPadViewController alloc] initWithNibName:nil bundle:nil];
    [self addChildViewController:informationController];
    informationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:[PJMenuInformationUtils menuTextForMenuInformation:informationTab]
                                                                     image:nil
                                                                       tag:menuTabIndex + 1];
}

-(NSArray*) infomationListWithMenuInfomation:(NSDictionary*) menuInfomation{
    NSMutableArray* list = @[].mutableCopy;
    [list addObject:menuInfomation[kBusinessKeyName]];
    [list addObject:menuInfomation[kSumKeyName]];
    [list addObject:menuInfomation[kReprintKeyName]];
    [list addObject:menuInfomation[kSettingsKeyName]];
    [list addObject:menuInfomation[kOtherKeyName]];
    return list;
}

@end
