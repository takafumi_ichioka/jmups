//
//  PJMenuBasePadViewController.h
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBasePadViewController.h"
#import "PJMenuInformation.h"

#define kMenuViewDiff 8

@interface PJMenuBasePadViewController : PJBasePadViewController

@property(nonatomic,readonly) CGRect menuViewFrame;

@property(nonatomic,readonly) NSString* menuTitle;
@property(nonatomic,readonly) NSArray*  menuList;

-(id) initWithMenuList:(NSArray*) menuList title:(NSString*) title;
-(void) selectedMenuInformation:(PJMenuInformation*) information;
-(void) didSelectedMenuInformation:(PJMenuInformation*) information;
-(void) menuClearSelect;

@end
