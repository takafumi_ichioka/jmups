//
//  PJMenuBasePadViewController.m
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJMenuBasePadViewController.h"
#import "PJMenuView.h"

@interface PJMenuBasePadViewController ()

@property(nonatomic,strong) NSArray* savedMenuList;
@property(nonatomic,strong) NSString* savedTitle;
@property(nonatomic,strong) PJMenuView* menuView;

@end

@implementation PJMenuBasePadViewController

-(id) initWithMenuList:(NSArray*) menuList title:(NSString *)title{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.savedMenuList = menuList.copy;
        self.savedTitle = title.copy;
    }
    return self;
}

-(void) loadView{
    [super loadView];
    self.menuView = [[PJMenuView alloc] initWithController:self
                                                 withTitle:self.savedTitle
                                              withMenuList:self.savedMenuList];
    __block PJMenuBasePadViewController* blockSelf = self;
    self.menuView.selectedHandler = ^(PJMenuInformation* menuInformation){
        [blockSelf didSelectedMenuInformation:menuInformation];
    };
}

-(NSArray*) menuList{
    return self.savedMenuList;
}

-(NSString*) menuTitle{
    return self.savedTitle;
}

-(CGRect) menuViewFrame{
    return self.menuView.frame;
}

-(void) selectedMenuInformation:(PJMenuInformation*) information{
    [self.menuView selectedMenuInformation:information];
}

-(void) menuClearSelect{
    [self.menuView clearSelect];
}

-(void) didSelectedMenuInformation:(PJMenuInformation*) information{
    
}

@end
