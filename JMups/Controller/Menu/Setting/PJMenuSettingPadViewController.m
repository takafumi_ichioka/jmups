//
//  PJMenuSettingPadViewController.m
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJMenuSettingPadViewController.h"
#import "PJPasswordUpdateView.h"

@interface PJMenuSettingPadViewController ()<PJPasswordUpdateViewDelegate>

@property(nonatomic,strong) PJPasswordUpdateView* passwordUpdateView;

@end

@implementation PJMenuSettingPadViewController

-(void) loadView{
    [super loadView];
    int index = 0;
    for (PJMenuInformation* menuInfo in self.menuList) {
        UIView* sendBackView;
        switch (index) {
            case 3:
                self.passwordUpdateView = [[PJPasswordUpdateView alloc] initWithController:self title:menuInfo.text];
                self.passwordUpdateView.delegate = self;
                self.passwordUpdateView.alpha = 0;
                sendBackView = self.passwordUpdateView;
                break;
            default:
                break;
        }
        [self.view sendSubviewToBack:sendBackView];
        index += 1;
    }
}

-(void) didSelectedMenuInformation:(PJMenuInformation *)information{
    CGRect menuViewFrame = self.menuViewFrame;
    UIView* openView;
    switch (information.index) {
        case 3:
            openView = self.passwordUpdateView;
            break;
        default:
            break;
    }
    CGRect openViewFrame = openView.frame;
    openViewFrame.origin.x = CGRectGetMaxX(menuViewFrame) - kMenuViewDiff;
    [UIView animateWithDuration:0.25 animations:^{
        openView.frame = openViewFrame;
        openView.alpha = 1.0;
    }];
}

-(void) didCompletedUpdatePassword:(PJPasswordUpdateView *)view{
    CGRect passwordUpdateViewFrame = self.passwordUpdateView.frame;
    passwordUpdateViewFrame.origin.x = 0;
    __weak PJMenuSettingPadViewController* weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.passwordUpdateView.frame = passwordUpdateViewFrame;
        weakSelf.passwordUpdateView.alpha = 0;
    } completion:^(BOOL finished){
        [weakSelf menuClearSelect];
    }];
}

-(void) didCancelUpdatePassword:(PJPasswordUpdateView *)view{
    CGRect passwordUpdateViewFrame = self.passwordUpdateView.frame;
    passwordUpdateViewFrame.origin.x = 0;
    __weak PJMenuSettingPadViewController* weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.passwordUpdateView.frame = passwordUpdateViewFrame;
        weakSelf.passwordUpdateView.alpha = 0;
    } completion:^(BOOL finished){
        [weakSelf menuClearSelect];
    }];
}

@end
