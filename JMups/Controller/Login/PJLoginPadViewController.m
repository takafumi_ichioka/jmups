//
//  LoginPadViewController.m
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJLoginPadViewController.h"
#import "PJLoginView.h"

@interface PJLoginPadViewController ()

@property(nonatomic,strong) PJLoginView* loginView;

@end

@implementation PJLoginPadViewController

-(void) loadView{
    [super loadView];
    _loginView = [[PJLoginView alloc] initWithOrigin:CGPointZero SuperView:self.view];
    _loginView.center = self.view.center;
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    _loginView.center = self.view.center;
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

@end
