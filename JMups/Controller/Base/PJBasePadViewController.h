//
//  BasePadViewController.h
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJViewUtils.h"
#import "NSString+JMups.h"

@interface PJBasePadViewController : UIViewController

-(void) settingViewWithOrientation:(UIInterfaceOrientation) orientation isRotating:(BOOL) isRotating;

@end
