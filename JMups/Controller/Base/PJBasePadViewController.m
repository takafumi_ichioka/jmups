//
//  BasePadViewController.m
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBasePadViewController.h"

@interface PJBasePadViewController ()

@property(nonatomic,strong) NSArray* savedAddingViews;

@end

@implementation PJBasePadViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

-(NSUInteger) supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

-(void) settingViewWithOrientation:(UIInterfaceOrientation) orientation isRotating:(BOOL) isRotating{
    
}

@end
