//
//  PJKeyPadUtils.m
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJKeyPadUtils.h"
#import "UIImage+JMups.h"
#import "NSString+JMups.h"

#define kKeyPadImageNameFormat @"key_ipad_%d.png"
#define kKeyPadOnImageNameFormat @"key_ipad_%d_on.png"

@implementation PJKeyPadUtils

+(UIImage*) keyPadImageWithNumber:(PJKeyPadNumber) number onImage:(BOOL) onImage{
    NSString* keyPadformat = onImage ? kKeyPadOnImageNameFormat : kKeyPadImageNameFormat;
    return [UIImage imageNamed:[NSString stringWithFormat:keyPadformat,number]];
}

+(void) tappedKeyPadNumber:(PJKeyPadNumber) number
               valueString:(NSString*) valueString
             tappedHandler:(KeyPadTappedHandler) handler{
    BOOL emptyValue = [NSString isNullOrEmptyString:valueString];
    if (emptyValue) {
        valueString = @"";
    }
    switch (number) {
        case PJKeyPadNumberZero:
        case PJKeyPadNumberOne:
        case PJKeyPadNumberTwo:
        case PJKeyPadNumberThree:
        case PJKeyPadNumberFour:
        case PJKeyPadNumberFive:
        case PJKeyPadNumberSix:
        case PJKeyPadNumberSeven:
        case PJKeyPadNumberEight:
        case PJKeyPadNumberNine:
            valueString = [valueString stringByAppendingFormat:@"%d",number];
            break;
        case PJKeyPadNumberBack:
            if (!emptyValue) {
                valueString = [valueString substringWithRange:NSMakeRange(0, valueString.length - 1)];
            }
            break;
        case PJKeyPadNumberClear:
            valueString = @"";
            break;
        case PJKeyPadNumberDoubleZero:
            valueString = [valueString stringByAppendingString:@"00"];
            break;
        default:
            break;
    }
    if (handler) {
        handler(valueString,number);
    }
}

@end
