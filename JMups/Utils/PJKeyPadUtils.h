//
//  PJKeyPadUtils.h
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSInteger{
    PJKeyPadNumberZero = 0,
    PJKeyPadNumberOne = 1,
    PJKeyPadNumberTwo = 2,
    PJKeyPadNumberThree = 3,
    PJKeyPadNumberFour = 4,
    PJKeyPadNumberFive = 5,
    PJKeyPadNumberSix = 6,
    PJKeyPadNumberSeven = 7,
    PJKeyPadNumberEight = 8,
    PJKeyPadNumberNine = 9,
    PJKeyPadNumberDecide = 10,
    PJKeyPadNumberBack = 11,
    PJKeyPadNumberClear = 12,
    PJKeyPadNumberDecideWide = 13,
    PJKeyPadNumberDoubleZero = 20
}PJKeyPadNumber;

typedef void(^KeyPadTappedHandler)(NSString* editString,PJKeyPadNumber inputNumber);

@interface PJKeyPadUtils : NSObject

+(UIImage*) keyPadImageWithNumber:(PJKeyPadNumber) number onImage:(BOOL) onImage;

+(void) tappedKeyPadNumber:(PJKeyPadNumber) number
               valueString:(NSString*) valueString
             tappedHandler:(KeyPadTappedHandler) handler;

@end
