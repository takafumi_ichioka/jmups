//
//  PJMenuInformationUtils.m
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJMenuInformationUtils.h"

#define kPlistFileKeyName               @"MenuInformation"
#define kMenuInformationTabsKeyName     @"MenuInformationTabs"
#define kInformationTabKeyName          @"InformationTab"
#define kBusinessKeyName                @"Business"
#define kSumKeyName                     @"Sum"
#define kReprintKeyName                 @"Reprint"
#define kSettingsKeyName                @"Settings"
#define kOtherKeyName                   @"Other"
#define kTextKeyName                    @"Text"
#define kMenuKeyName                    @"Menu"
#define kChildrenKeyName                @"Children"

@implementation PJMenuInformationUtils

+(NSDictionary*) menuInformationForKey:(NSString*) key{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:kPlistFileKeyName ofType:@"plist"];
    NSDictionary* menuInformations = [NSDictionary dictionaryWithContentsOfFile:filePath];
    return menuInformations[key];
}

+(NSString*) menuTextForMenuInformation:(NSDictionary*) information{
    return information[kTextKeyName];
}
+(NSArray*) childrenForMenuInformation:(NSDictionary *)information{
    return information[kChildrenKeyName];
}

+(NSArray*)  menusForMenuInformation:(NSDictionary*) information{
    return information[kMenuKeyName];
}

+(PJMenuInformation*) generateMenuInfomationWithMenu:(NSDictionary*) menu index:(NSInteger)index{
    PJMenuInformation* menuInfomation = [[PJMenuInformation alloc] init];
    menuInfomation.text = menu[kTextKeyName];
    menuInfomation.index = index;
    NSArray* children = menu[kChildrenKeyName];
    if (children) {
        int childIndex = 0;
        for (NSString* childText in children) {
            PJMenuInformation* childMenu = [[PJMenuInformation alloc] init];
            childMenu.text = childText;
            childMenu.index = childIndex;
            [menuInfomation addChild:childMenu];
            childIndex += 1;
        }
    }
    return menuInfomation;
}

@end
