//
//  PJMenuInformationUtils.h
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJMenuInformation.h"

@interface PJMenuInformationUtils : NSObject

+(NSDictionary*) menuInformationForKey:(NSString*) key;
+(NSString*) menuTextForMenuInformation:(NSDictionary*) information;
+(NSArray*)  childrenForMenuInformation:(NSDictionary*) information;
+(NSArray*)  menusForMenuInformation:(NSDictionary*) information;
+(PJMenuInformation*) generateMenuInfomationWithMenu:(NSDictionary*) menu index:(NSInteger) index;

@end
