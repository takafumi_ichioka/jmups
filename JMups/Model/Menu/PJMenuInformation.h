//
//  PJMenuInformation.h
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PJMenuInformation : NSObject

@property(nonatomic,copy) NSString* text;
@property(nonatomic)      NSInteger index;
@property(nonatomic,weak) PJMenuInformation* parent;
@property(nonatomic,readonly) NSArray* children;
-(void) addChild:(PJMenuInformation*) child;

@end
