//
//  PJMenuInformation.m
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJMenuInformation.h"

@interface PJMenuInformation ()

@property(nonatomic,strong) NSMutableArray* addingChildren;

@end

@implementation PJMenuInformation

-(id) init{
    self = [super init];
    if (self) {
        self.text = @"";
        self.addingChildren = @[].mutableCopy;
        self.index = -1;
    }
    return self;
}

-(NSArray*) children{
    return self.addingChildren;
}

-(void) addChild:(PJMenuInformation *)child{
    child.parent = self;
    [self.addingChildren addObject:child];
}

@end
