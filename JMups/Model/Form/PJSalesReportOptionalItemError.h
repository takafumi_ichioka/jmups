/*
 *  PJSalesReportOptionalItemError.h
 *  PaymentMeister
 *  合計チェックエラー売上報告項目情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>


@interface PJSalesReportOptionalItemError : NSObject

@property (nonatomic, retain) NSString *frmOptionalItemNo;
@property (nonatomic, retain) NSString *frmOptionalItemName;
@property (nonatomic, retain) NSString *frmOptionalItemValue;
@property (nonatomic, retain) NSString *frmOptionalItemType;

@end
