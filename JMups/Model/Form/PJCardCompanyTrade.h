/*
 *  PJCardCompanyTrade.h
 *  PaymentMeister
 *  カード会社取引情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>


@interface PJCardCompanyTrade : NSObject

@property (nonatomic, retain) NSString *frmTrade;
@property (nonatomic, retain) NSString *frmAmount;
@property (nonatomic, retain) NSString *frmPayDivision;
@property (nonatomic, retain) NSString *frmTradeDate;
@property (nonatomic, retain) NSString *frmCardNo;
@property (nonatomic, retain) NSString *frmExpDate;
@property (nonatomic, retain) NSString *frmSlipNo;
@property (nonatomic, retain) NSString *frmANo;

@end
