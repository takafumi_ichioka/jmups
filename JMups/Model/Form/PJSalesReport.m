/*
 *  PJSalesReport.m
 *  PaymentMeister
 *  売上報告（エラー）伝票情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJSalesReport.h"


@implementation PJSalesReport

@dynamic frmTitle;
@dynamic frmMerchantName;
@dynamic frmMerchantTelNo;
@dynamic frmTermNo;
@dynamic frmProcessDate;
@dynamic frmDate;
@dynamic frmStoreCode;
@dynamic frmCounterCode;
@dynamic frmStaffCode;
@dynamic frmMessage1;
@dynamic frmMessage2;
@dynamic frmIsTraining;
@dynamic frmIsreprint;
@dynamic frmOptionalItemList;
@dynamic frmOptionalItemErrorList;

@end
