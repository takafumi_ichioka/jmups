/*
 *  PJLogoServiceInfo.h
 *  PaymentMeister
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>


@interface PJLogoServiceInfo : NSObject

@property (nonatomic, retain) NSString *frmLogoImageServiceInfo;
@property (nonatomic, retain) NSString *frmLogoSwitchDate;

@end
