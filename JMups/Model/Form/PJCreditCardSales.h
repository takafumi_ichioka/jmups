/*
 *  PJCreditCardSales.h
 *  PaymentMeister
 *  クレジットカード売上票情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>
#import "PJLogoServiceInfo.h"


@interface PJCreditCardSales : NSObject

@property (nonatomic, retain) NSString *frmIsTraining;
@property (nonatomic, retain) NSString *frmTitle;
@property (nonatomic, retain) NSString *frmIsReprint;
@property (nonatomic, retain) NSString *frmMerchantName;
@property (nonatomic, retain) NSString *frmMerchantTelNo;
@property (nonatomic, retain) NSString *frmDate;
@property (nonatomic, retain) NSString *frmCardCo;
@property (nonatomic, retain) NSString *frmCardNo;
@property (nonatomic, retain) NSString *frmTermNo;
@property (nonatomic, retain) NSString *frmSlipNo;
@property (nonatomic, retain) NSString *frmExpDate;
@property (nonatomic, retain) NSString *frmANo;
@property (nonatomic, retain) NSString *frmPayDivision;
@property (nonatomic, retain) NSString *frmProductDivision;
@property (nonatomic, retain) NSString *frmBonus;
@property (nonatomic, retain) NSString *frmInstallment;
@property (nonatomic, retain) NSString *frmAmount;
@property (nonatomic, retain) NSString *frmTax;
@property (nonatomic, retain) NSString *frmTotalYen;
@property (nonatomic, retain) NSString *frmInform1;
@property (nonatomic, retain) NSString *frmInform2;
@property (nonatomic, retain) NSString *frmInform3;
@property (nonatomic, retain) NSString *frmSign;
@property (nonatomic, retain) NSString *frmIsEnglish;
@property (nonatomic, retain) NSString *frmTradeDiv;
@property (nonatomic, retain) NSString *frmReceipt;
@property (nonatomic, retain) NSString *frmCoupon1;
@property (nonatomic, retain) NSString *frmCoupon2;
@property (nonatomic, retain) NSString *frmCardDiv;
@property (nonatomic, retain) NSString *frmDiscountPrice;
@property (nonatomic, retain) NSString *frmArc;
@property (nonatomic, retain) NSString *frmAtc;
@property (nonatomic, retain) NSString *frmCouponId;
@property (nonatomic, retain) NSString *frmPhysicalMediaDiv;
@property (nonatomic, retain) NSString *frmCouponMemberId;
@property (nonatomic, retain) NSString *frmCouponApplyPreviousAmount;
@property (nonatomic, retain) NSString *frmMaximumUsePossTimes;
@property (nonatomic, retain) NSString *frmUseDoneTimes;
@property (nonatomic, retain) NSString *frmAid;
@property (nonatomic, retain) NSString *frmCardSeqNo;
@property (nonatomic, retain) PJLogoServiceInfo *frmLogoServiceInfo;
@property (nonatomic, retain) NSString *frmFirstTimeClaimMonth;
@property (nonatomic, retain) NSString *frmBonusTimes;
@property (nonatomic, retain) NSString *frmBonusMonth1;
@property (nonatomic, retain) NSString *frmBonusMonth2;
@property (nonatomic, retain) NSString *frmPoint;

@end
