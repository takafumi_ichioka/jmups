/*
 *  PJCardCheckResult.h
 *  PaymentMeister
 *  カードチェック票情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>
#import "PJLogoServiceInfo.h"


@interface PJCardCheckResult : NSObject

@property (nonatomic, retain) NSString *frmIsTraining;
@property (nonatomic, retain) NSString *frmTermNo;
@property (nonatomic, retain) NSString *frmTrade;
@property (nonatomic, retain) NSString *frmCardCo;
@property (nonatomic, retain) NSString *frmReceipt;
@property (nonatomic, retain) NSString *frmAmount;
@property (nonatomic, retain) NSString *frmResult;
@property (nonatomic, retain) NSString *frmExpDate;
@property (nonatomic, retain) NSString *frmCardNo;
@property (nonatomic, retain) NSString *frmDate;
@property (nonatomic, retain) NSString *frmSlipNo;
@property (nonatomic, retain) NSString *frmMerchantName;
@property (nonatomic, retain) NSString *frmIsReprint;
@property (nonatomic, retain) NSString *frmANo;
@property (nonatomic, retain) NSString *frmMerchantTelNo;
@property (nonatomic, retain) NSString *frmTitle;
@property (nonatomic, retain) PJLogoServiceInfo *frmLogoServiceInfo;

@end
