/*
 *  PJTerminalServicesConfiguration.m
 *  PaymentMeister
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJTerminalServicesConfiguration.h"


@implementation PJTerminalServicesConfiguration

@dynamic frmTermNo;
@dynamic frmDate;
@dynamic frmTitle;
@dynamic frmCompanyList;
@dynamic frmLogoServiceInfo;

@end
