/*
 *  PJCreditCardIntermediateTotalDetails
 *  PaymentMeister
 *  クレジットカード中間計票（詳細）情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJCreditCardIntermediateTotalDetails.h"


@implementation PJCreditCardIntermediateTotalDetails

@dynamic frmTitle;
@dynamic frmMerchantName;
@dynamic frmMerchantTelNo;
@dynamic frmDate;
@dynamic frmTermNo;
@dynamic frmDrDateFrom;
@dynamic frmDrDateTo;
@dynamic frmT1xNumber;
@dynamic frmT1xAmount;
@dynamic frmT2xNumber;
@dynamic frmT2xAmount;
@dynamic frmT3xNumber;
@dynamic frmT3xAmount;
@dynamic frmT6xNumber;
@dynamic frmT6xAmount;
@dynamic frmT8xNumber;
@dynamic frmT8xAmount;
@dynamic frmTSaNumber;
@dynamic frmTSaAmount;
@dynamic frmTCaNumber;
@dynamic frmTCaAmount;
@dynamic frmTReNumber;
@dynamic frmTReAmount;
@dynamic frmTTotalNumber;
@dynamic frmTTotalAmount;
@dynamic frmIsReprint;
@dynamic frmIsTraining;
@dynamic frmJournalDate;
@dynamic frmCompanyList;
@dynamic frmLogoServiceInfo;


@end
