/*
 *  PJCardCompany.h
 *  PaymentMeister
 *  カード会社情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>


@interface PJCardCompany : NSObject

@property (nonatomic, retain) NSString *frmCoName;
@property (nonatomic, retain) NSString *frmC1xNumber;
@property (nonatomic, retain) NSString *frmC1xAmount;
@property (nonatomic, retain) NSString *frmC2xNumber;
@property (nonatomic, retain) NSString *frmC2xAmount;
@property (nonatomic, retain) NSString *frmC3xNumber;
@property (nonatomic, retain) NSString *frmC3xAmount;
@property (nonatomic, retain) NSString *frmC6xNumber;
@property (nonatomic, retain) NSString *frmC6xAmount;
@property (nonatomic, retain) NSString *frmC8xNumber;
@property (nonatomic, retain) NSString *frmC8xAmount;
@property (nonatomic, retain) NSString *frmCSaNumber;
@property (nonatomic, retain) NSString *frmCSaAmount;
@property (nonatomic, retain) NSString *frmCCaNumber;
@property (nonatomic, retain) NSString *frmCCaAmount;
@property (nonatomic, retain) NSString *frmCReNumber;
@property (nonatomic, retain) NSString *frmCReAmount;
@property (nonatomic, retain) NSString *frmCTotalNumber;
@property (nonatomic, retain) NSString *frmCTotalAmount;
@property (nonatomic, retain) NSArray *frmTradeList;

@end
