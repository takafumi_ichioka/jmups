/*
 *  PJCardCompanyTrade.m
 *  PaymentMeister
 *  カード会社取引情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJCardCompanyTrade.h"


@implementation PJCardCompanyTrade

@dynamic frmTrade;
@dynamic frmAmount;
@dynamic frmPayDivision;
@dynamic frmTradeDate;
@dynamic frmCardNo;
@dynamic frmExpDate;
@dynamic frmSlipNo;
@dynamic frmANo;

@end
