/*
 *  PJCreditCardDailyTotalDetails.h
 *  PaymentMeister
 *  クレジットカード日計票（詳細）情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>
#import "PJLogoServiceInfo.h"


@interface PJCreditCardDailyTotalDetails : NSObject

@property (nonatomic, retain) NSString *frmTitle;
@property (nonatomic, retain) NSString *frmMerchantName;
@property (nonatomic, retain) NSString *frmMerchantTelNo;
@property (nonatomic, retain) NSString *frmDate;
@property (nonatomic, retain) NSString *frmTermNo;
@property (nonatomic, retain) NSString *frmDrDateFrom;
@property (nonatomic, retain) NSString *frmDrDateTo;
@property (nonatomic, retain) NSString *frmT1xNumber;
@property (nonatomic, retain) NSString *frmT1xAmount;
@property (nonatomic, retain) NSString *frmT2xNumber;
@property (nonatomic, retain) NSString *frmT2xAmount;
@property (nonatomic, retain) NSString *frmT3xNumber;
@property (nonatomic, retain) NSString *frmT3xAmount;
@property (nonatomic, retain) NSString *frmT6xNumber;
@property (nonatomic, retain) NSString *frmT6xAmount;
@property (nonatomic, retain) NSString *frmT8xNumber;
@property (nonatomic, retain) NSString *frmT8xAmount;
@property (nonatomic, retain) NSString *frmTSaNumber;
@property (nonatomic, retain) NSString *frmTSaAmount;
@property (nonatomic, retain) NSString *frmTReNumber;
@property (nonatomic, retain) NSString *frmTReAmount;
@property (nonatomic, retain) NSString *frmTCaNumber;
@property (nonatomic, retain) NSString *frmTCaAmount;
@property (nonatomic, retain) NSString *frmTTotalNumber;
@property (nonatomic, retain) NSString *frmTTotalAmount;
@property (nonatomic, retain) NSString *frmIsReprint;
@property (nonatomic, retain) NSString *frmIsTraining;
@property (nonatomic, retain) NSString *frmJournalDate;
@property (nonatomic, retain) NSArray *frmCompanyList;
@property (nonatomic, retain) PJLogoServiceInfo *frmLogoServiceInfo;

@end
