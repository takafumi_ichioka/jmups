/*
 *  PJSalesReportOptionalItem.m
 *  PaymentMeister
 *  売上報告項目情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJSalesReportOptionalItem.h"


@implementation PJSalesReportOptionalItem

@dynamic frmOptionalItemNo;
@dynamic frmOptionalItemName;
@dynamic frmOptionalItemValue;
@dynamic frmOptionalItemType;
@dynamic frmErrorReportTag;

@end
