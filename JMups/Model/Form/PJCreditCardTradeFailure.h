/*
 *  PJCreditCardTradeFailure.h
 *  PaymentMeister
 *  クレジットカード取引不成立票情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>
#import "PJLogoServiceInfo.h"


@interface PJCreditCardTradeFailure : NSObject

@property (nonatomic, retain) NSString *frmTitle;
@property (nonatomic, retain) NSString *frmMerchantName;
@property (nonatomic, retain) NSString *frmMerchantTelNo;
@property (nonatomic, retain) NSString *frmDate;
@property (nonatomic, retain) NSString *frmCardCo;
@property (nonatomic, retain) NSString *frmCardNo;
@property (nonatomic, retain) NSString *frmTermNo;
@property (nonatomic, retain) NSString *frmSlipNo;
@property (nonatomic, retain) NSString *frmExpDate;
@property (nonatomic, retain) NSString *frmANo;
@property (nonatomic, retain) NSString *frmPayDivision;
@property (nonatomic, retain) NSString *frmProductDivision;
@property (nonatomic, retain) NSString *frmTrade;
@property (nonatomic, retain) NSString *frmFailureInfo;
@property (nonatomic, retain) NSString *frmReceipt;
@property (nonatomic, retain) NSString *frmIsReprint;
@property (nonatomic, retain) NSString *frmIsTraining;
@property (nonatomic, retain) NSString *frmPhysicalMediaDiv;
@property (nonatomic, retain) NSString *frmTradeDiv;
@property (nonatomic, retain) NSString *frmCardDiv;
@property (nonatomic, retain) NSString *frmArc;
@property (nonatomic, retain) NSString *frmAid;
@property (nonatomic, retain) NSString *frmCardSeqNo;
@property (nonatomic, retain) NSString *frmAtc;
@property (nonatomic, retain) NSString *frmAmount;
@property (nonatomic, retain) PJLogoServiceInfo *frmLogoServiceInfo;

@end
