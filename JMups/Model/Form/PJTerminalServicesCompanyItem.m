/*
 *  PJTerminalServicesCompanyItem.m
 *  PaymentMeister
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJTerminalServicesCompanyItem.h"


@implementation PJTerminalServicesCompanyItem

@dynamic frmServiceContractorName;
@dynamic frmServiceContractorId;
@dynamic frmServiceDivName;

@end
