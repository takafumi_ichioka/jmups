/*
 *  PJCardCheckResult.m
 *  PaymentMeister
 *  カードチェック票情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJCardCheckResult.h"


@implementation PJCardCheckResult

@dynamic frmIsTraining;
@dynamic frmTermNo;
@dynamic frmTrade;
@dynamic frmCardCo;
@dynamic frmReceipt;
@dynamic frmAmount;
@dynamic frmResult;
@dynamic frmExpDate;
@dynamic frmCardNo;
@dynamic frmDate;
@dynamic frmSlipNo;
@dynamic frmMerchantName;
@dynamic frmIsReprint;
@dynamic frmANo;
@dynamic frmMerchantTelNo;
@dynamic frmTitle;
@dynamic frmLogoServiceInfo;

@end
