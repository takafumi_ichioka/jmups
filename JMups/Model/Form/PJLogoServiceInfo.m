/*
 *  PJLogoServiceInfo.m
 *  PaymentMeister
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJLogoServiceInfo.h"


@implementation PJLogoServiceInfo

@dynamic frmLogoImageServiceInfo;
@dynamic frmLogoSwitchDate;

@end
