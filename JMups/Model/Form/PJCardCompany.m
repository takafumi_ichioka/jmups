/*
 *  PJCardCompany.m
 *  PaymentMeister
 *  カード会社情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJCardCompany.h"


@implementation PJCardCompany

@dynamic frmCoName;
@dynamic frmC1xNumber;
@dynamic frmC1xAmount;
@dynamic frmC2xNumber;
@dynamic frmC2xAmount;
@dynamic frmC3xNumber;
@dynamic frmC3xAmount;
@dynamic frmC6xNumber;
@dynamic frmC6xAmount;
@dynamic frmC8xNumber;
@dynamic frmC8xAmount;
@dynamic frmCSaNumber;
@dynamic frmCSaAmount;
@dynamic frmCCaNumber;
@dynamic frmCCaAmount;
@dynamic frmCReNumber;
@dynamic frmCReAmount;
@dynamic frmCTotalNumber;
@dynamic frmCTotalAmount;
@dynamic frmTradeList;

@end
