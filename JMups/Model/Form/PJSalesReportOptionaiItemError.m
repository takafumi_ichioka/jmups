/*
 *  PJSalesReportOptionalItemError.m
 *  PaymentMeister
 *  合計チェックエラー売上報告項目情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJSalesReportOptionalItemError.h"


@implementation PJSalesReportOptionalItemError

@dynamic frmOptionalItemNo;
@dynamic frmOptionalItemName;
@dynamic frmOptionalItemValue;
@dynamic frmOptionalItemType;

@end
