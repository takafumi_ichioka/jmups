/*
 *  PJTerminalServicesCompanyItem.h
 *  PaymentMeister
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>


@interface PJTerminalServicesCompanyItem : NSObject

@property (nonatomic, retain) NSString *frmServiceContractorName;
@property (nonatomic, retain) NSString *frmServiceContractorId;
@property (nonatomic, retain) NSString *frmServiceDivName;

@end
