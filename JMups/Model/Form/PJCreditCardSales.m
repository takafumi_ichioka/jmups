/*
 *  PJCreditCardSales.m
 *  PaymentMeister
 *  クレジットカード売上票情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJCreditCardSales.h"


@implementation PJCreditCardSales

@dynamic frmIsTraining;
@dynamic frmTitle;
@dynamic frmIsReprint;
@dynamic frmMerchantName;
@dynamic frmMerchantTelNo;
@dynamic frmDate;
@dynamic frmCardCo;
@dynamic frmCardNo;
@dynamic frmTermNo;
@dynamic frmSlipNo;
@dynamic frmExpDate;
@dynamic frmANo;
@dynamic frmPayDivision;
@dynamic frmProductDivision;
@dynamic frmBonus;
@dynamic frmInstallment;
@dynamic frmAmount;
@dynamic frmTax;
@dynamic frmTotalYen;
@dynamic frmInform1;
@dynamic frmInform2;
@dynamic frmInform3;
@dynamic frmSign;
@dynamic frmIsEnglish;
@dynamic frmTradeDiv;
@dynamic frmReceipt;
@dynamic frmCoupon1;
@dynamic frmCoupon2;
@dynamic frmCardDiv;
@dynamic frmDiscountPrice;
@dynamic frmArc;
@dynamic frmAtc;
@dynamic frmCouponId;
@dynamic frmPhysicalMediaDiv;
@dynamic frmCouponMemberId;
@dynamic frmCouponApplyPreviousAmount;
@dynamic frmMaximumUsePossTimes;
@dynamic frmUseDoneTimes;
@dynamic frmAid;
@dynamic frmCardSeqNo;
@dynamic frmLogoServiceInfo;
@dynamic frmFirstTimeClaimMonth;
@dynamic frmBonusTimes;
@dynamic frmBonusMonth1;
@dynamic frmBonusMonth2;
@dynamic frmPoint;

@end
