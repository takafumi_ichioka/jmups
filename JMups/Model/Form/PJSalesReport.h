/*
 *  PJSalesReport.h
 *  PaymentMeister
 *  売上報告（エラー）伝票情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>


@interface PJSalesReport : NSObject

@property (nonatomic, retain) NSString *frmTitle;
@property (nonatomic, retain) NSString *frmMerchantName;
@property (nonatomic, retain) NSString *frmMerchantTelNo;
@property (nonatomic, retain) NSString *frmTermNo;
@property (nonatomic, retain) NSString *frmProcessDate;
@property (nonatomic, retain) NSString *frmDate;
@property (nonatomic, retain) NSString *frmStoreCode;
@property (nonatomic, retain) NSString *frmCounterCode;
@property (nonatomic, retain) NSString *frmStaffCode;
@property (nonatomic, retain) NSString *frmMessage1;
@property (nonatomic, retain) NSString *frmMessage2;
@property (nonatomic, retain) NSString *frmIsTraining;
@property (nonatomic, retain) NSString *frmIsreprint;
@property (nonatomic, retain) NSArray *frmOptionalItemList;
@property (nonatomic, retain) NSArray *frmOptionalItemErrorList;

@end
