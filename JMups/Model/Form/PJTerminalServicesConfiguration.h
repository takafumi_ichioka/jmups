/*
 *  PJTerminalServicesConfiguration.h
 *  PaymentMeister
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>
#import "PJLogoServiceInfo.h"


@interface PJTerminalServicesConfiguration : NSObject

@property (nonatomic, retain) NSString *frmTermNo;
@property (nonatomic, retain) NSString *frmDate;
@property (nonatomic, retain) NSString *frmTitle;
@property (nonatomic, retain) NSArray *frmCompanyList;
@property (nonatomic, retain) PJLogoServiceInfo *frmLogoServiceInfo;

@end
