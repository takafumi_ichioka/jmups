/*
 *  PJCreditCardTradeFailure.m
 *  PaymentMeister
 *  クレジットカード取引不成立票情報
 *
 *  Created by FLIGHT on 14/01/23.
 *  Copyright 2013 FLIGHT SYSTEM CONSULTING Inc.. All rights reserved.
 *
 */
#import "PJCreditCardTradeFailure.h"


@implementation PJCreditCardTradeFailure

@dynamic frmTitle;
@dynamic frmMerchantName;
@dynamic frmMerchantTelNo;
@dynamic frmDate;
@dynamic frmCardCo;
@dynamic frmCardNo;
@dynamic frmTermNo;
@dynamic frmSlipNo;
@dynamic frmExpDate;
@dynamic frmANo;
@dynamic frmPayDivision;
@dynamic frmProductDivision;
@dynamic frmTrade;
@dynamic frmFailureInfo;
@dynamic frmReceipt;
@dynamic frmIsReprint;
@dynamic frmIsTraining;
@dynamic frmPhysicalMediaDiv;
@dynamic frmTradeDiv;
@dynamic frmCardDiv;
@dynamic frmArc;
@dynamic frmAid;
@dynamic frmCardSeqNo;
@dynamic frmAtc;
@dynamic frmAmount;
@dynamic frmLogoServiceInfo;

@end
