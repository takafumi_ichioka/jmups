//
//  AppDelegate.h
//  JMups
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJLibInterface.h"

@interface PJAppDelegate : UIResponder <UIApplicationDelegate,PJLibInterfaceDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
