//
//  PJProgressView.m
//  JMups
//
//  Created by flight on 2014/01/28.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJProgressView.h"
#import <QuartzCore/QuartzCore.h>

#define kBaseProgressViewWith 350
#define kBaseProgressMarginWitdh 25
#define kBaseProgressMarginHeight 25
#define kMessageLabelMarginHeight 20
#define kMessageLabelFontSize 20
#define kIndicatorSize 60
#define kProgressBarHeight 8

@interface PJProgressView ()

@property(nonatomic,strong) UIView* baseProgressView;
@property(nonatomic,strong) UIView* backgroundView;
@property(nonatomic,strong) UILabel* messageLabel;
@property(nonatomic,strong) UIActivityIndicatorView* indicator;
@property(nonatomic,strong) UIProgressView*          progressView;
@property(nonatomic,copy)   ProgressStopHandler      stopHandler;

@end

@implementation PJProgressView

-(id) initWithSuperView:(UIView*) superView
                message:(NSString*) message
           progressType:(PJProgressViewType) progressType{
    CGFloat superViewWidth = CGRectGetWidth(superView.frame);
    CGFloat superViewHeight = CGRectGetHeight(superView.frame);
    CGRect frame = CGRectMake(0, 0, superViewWidth, superViewHeight);
    self = [super initWithFrame:frame superView:superView];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.backgroundView = [PJViewUtils generateViewWithFrame:frame
                                                       superView:self
                                                 backgroundColor:[UIColor colorWithWhite:0.1 alpha:0.8]];
        self.baseProgressView = [PJViewUtils generateViewWithFrame:frame
                                                         superView:self
                                                   backgroundColor:[UIColor colorWithWhite:0.8 alpha:0.95]];
        self.progressType = progressType;
        self.message = message;
        self.hidden = YES;
    }
    return self;
}

-(void) setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.backgroundView.frame = CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame));
}

-(void) setMessage:(NSString *)message{
   [self editBaseProgressViewSubViewWithMessage:message];
}

-(NSString*) message{
    return self.messageLabel.text;
}

-(void) setBarProgress:(CGFloat)barProgress{
    if (self.progressView) {
        self.progressView.progress = barProgress;
    }
}

-(void) startWithStopHandler:(ProgressStopHandler) handler{
    self.stopHandler = handler;
    CGFloat labelButtom = CGRectGetMaxY(self.messageLabel.frame);
    CGFloat baseViewWidth = kBaseProgressViewWith;
    CGFloat baseViewHeight = labelButtom + kBaseProgressMarginHeight;
    if (self.progressType == PJProgressViewTypeNone) {
        [self indicatorStop];
        [self progressStop];
    } else {
        if (self.progressType == PJProgressViewTypeMulti) {
            CGPoint indicatorCenter = CGPointMake(baseViewWidth / 2, labelButtom + (kIndicatorSize / 2));
            [self settingIndicatorWithCenter:indicatorCenter];
            CGRect progressFrame = CGRectMake(kBaseProgressMarginWitdh, labelButtom + kIndicatorSize, [self baseProgressSubViewWidth], kProgressBarHeight);
            [self settingProgressViewWithFrame:progressFrame];
            baseViewHeight += kIndicatorSize + kProgressBarHeight;
            
        } else if (self.progressType == PJProgressViewTypeIndicator) {
            CGPoint indicatorCenter = CGPointMake(baseViewWidth / 2, labelButtom + (kIndicatorSize / 2));
            [self settingIndicatorWithCenter:indicatorCenter];
            [self progressStop];
            
            baseViewHeight += kIndicatorSize;
        } else if (self.progressType == PJProgressViewTypeBar) {
            [self indicatorStop];
            CGRect progressFrame = CGRectMake(kBaseProgressMarginWitdh, labelButtom, [self baseProgressSubViewWidth], kProgressBarHeight);
            [self settingProgressViewWithFrame:progressFrame];
            baseViewHeight += kProgressBarHeight;
        }
        
    }
    CGFloat baseViewX = (CGRectGetWidth(self.backgroundView.frame) - baseViewWidth) / 2;
    CGFloat baseViewY = (CGRectGetHeight(self.backgroundView.frame) - baseViewHeight) / 2;
    self.baseProgressView.frame = CGRectMake(baseViewX, baseViewY, baseViewWidth, baseViewHeight);
    self.baseProgressView.layer.borderWidth = 2;
    self.baseProgressView.layer.borderColor = [UIColor colorWithWhite:0.1 alpha:1.0].CGColor;
    self.baseProgressView.layer.cornerRadius = 4;
    self.superview.userInteractionEnabled = NO;
    self.hidden = NO;
}

-(void) stop{
    self.superview.userInteractionEnabled = YES;
    self.hidden = YES;
    [self indicatorStop];
    [self progressStop];
    if (self.stopHandler) {
        self.stopHandler();
    }
}

-(void) settingIndicatorWithCenter:(CGPoint) center{
    if (self.indicator) {
        self.indicator.center = center;
        self.indicator.hidden = NO;
        [self.indicator startAnimating];
    } else {
        self.indicator = [PJViewUtils generateIndicatorWithStyle:UIActivityIndicatorViewStyleWhiteLarge
                                                       superView:self.baseProgressView
                                                          center:center
                                                        starting:YES];
    }
}

-(void) settingProgressViewWithFrame:(CGRect) frame{
    if (self.progressView) {
        self.progressView.frame = frame;
        self.progressView.progress = 0;
        self.progressView.hidden = NO;
    } else {
        self.progressView = [PJViewUtils genetateProgressViewWithStyle:UIProgressViewStyleDefault
                                                                 frame:frame
                                                             superView:self.baseProgressView
                                                              progress:0.0];
        self.progressView.tintColor = [UIColor blueColor];
        self.progressView.trackTintColor = [UIColor whiteColor];
    }
}

-(void) progressStop{
    if (self.progressView) {
        self.progressView.progress = 0;
        self.progressView.hidden = YES;
    }
}

-(void) indicatorStop{
    if (self.indicator) {
        [self.indicator stopAnimating];
        self.indicator.hidden = YES;
    }
}
                              
-(UIFont*) messageFont{
    return [UIFont boldSystemFontOfSize:kMessageLabelFontSize];
}

-(CGFloat) baseProgressSubViewWidth{
    return kBaseProgressViewWith - (kBaseProgressMarginWitdh * 2);
}

-(void) editBaseProgressViewSubViewWithMessage:(NSString*) message{
    CGSize messageSize = [self sizeWithMessage:message];
    CGFloat labelTop = kBaseProgressMarginHeight;
    CGFloat labelHeight = floorf(messageSize.height) + kMessageLabelMarginHeight;
    CGFloat baseSubViewWidth = [self baseProgressSubViewWidth];
    CGRect messageLabelFrame = CGRectMake(kBaseProgressMarginWitdh, labelTop,baseSubViewWidth,labelHeight);
    if (self.messageLabel) {
        self.messageLabel.text = message;
        self.messageLabel.frame = messageLabelFrame;
    } else {
        self.messageLabel = [PJViewUtils generateLabelWithFrame:messageLabelFrame
                                                      superView:self.baseProgressView
                                                           text:message
                                                      textColor:[UIColor blackColor]
                                                  textAlignment:NSTextAlignmentLeft
                                                           font:[self messageFont]
                                                  linebreakMode:NSLineBreakByWordWrapping
                                                  numberOfLines:0];
    }
}

-(CGSize) sizeWithMessage:(NSString*) message{
    return [message boundingRectWithSize:CGSizeMake([self baseProgressSubViewWidth],CGRectGetHeight(self.frame))
                                 options:NSStringDrawingUsesLineFragmentOrigin
                              attributes:@{NSFontAttributeName:[self messageFont]}
                                 context:nil].size;
}

@end
