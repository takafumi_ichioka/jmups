//
//  PJProgressView.h
//  JMups
//
//  Created by flight on 2014/01/28.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"

typedef enum{
    PJProgressViewTypeNone = 1,
    PJProgressViewTypeIndicator,
    PJProgressViewTypeBar,
    PJProgressViewTypeMulti
}PJProgressViewType;

typedef void (^ProgressStopHandler)();

@interface PJProgressView : PJBaseCustomView

-(id) initWithSuperView:(UIView*) superView
                message:(NSString*) message
           progressType:(PJProgressViewType) progressType;

@property(nonatomic,copy) NSString* message;
@property(nonatomic)      PJProgressViewType progressType;
@property(nonatomic)      CGFloat barProgress;

-(void) startWithStopHandler:(ProgressStopHandler) handler;
-(void) stop;

@end
