//
//  PJDoubleTextView.h
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"

@interface PJDoubleTextFieldView : PJBaseCustomView

+(UIImage*) doubleTextBackImage;

-(id) initWithOrigin:(CGPoint) origin superView:(UIView*) view;

@property(nonatomic,copy) NSString* upperText;
@property(nonatomic,copy) UIColor*  upperTextColor;
@property(nonatomic,copy) NSString* upperPlaceHolder;
@property(nonatomic)      BOOL      upperPassword;
@property(nonatomic,copy) NSString* underText;
@property(nonatomic,copy) UIColor*  underTextColor;
@property(nonatomic,copy) NSString* underPlaceHolder;
@property(nonatomic)      BOOL      underPassword;
@property(nonatomic)      BOOL      acceptKeyboard;

-(BOOL) isMatchDoubleText;

@end
