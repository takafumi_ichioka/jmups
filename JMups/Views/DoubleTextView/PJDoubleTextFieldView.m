//
//  PJDoubleTextView.m
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJDoubleTextFieldView.h"

#define kDoubleBoxImageName @"box_double.png"
#define kDoubleBoxUpperImageName @"box_double_upper.png"
#define kDoubleBoxUnderImageName @"box_double_under.png"

#define kTextFieldMarginHeight  1
#define kTextFieldMarginWidth   3
#define kTextFieldHeight        43
#define kSeparaorLineHeight     1

#define kTextFieldFontSize      14

@interface PJDoubleTextFieldView ()<UITextFieldDelegate>

@property(nonatomic,strong) UIImageView* backImageView;
@property(nonatomic,strong) UITextField* upperTextField;
@property(nonatomic,strong) UITextField* underTextField;

@end
@implementation PJDoubleTextFieldView

+(UIImage*) doubleTextBackImage{
    return [UIImage imageNamed:kDoubleBoxImageName];
}

-(id) initWithOrigin:(CGPoint)origin superView:(UIView *)view{
    UIImage* backImage = [PJDoubleTextFieldView doubleTextBackImage];
    CGFloat selfWidth = backImage.size.width;
    CGFloat selfHeight = backImage.size.height;
    CGRect frame = CGRectMake(origin.x, origin.y, selfWidth, selfHeight);
    self = [super initWithFrame:frame superView:view];
    if (self) {
        self.backImageView = [PJViewUtils generateImageViewWithImage:backImage
                                                               frame:CGRectMake(0, 0, selfWidth, selfHeight)
                                                           superView:self
                                                         contentMode:UIViewContentModeScaleToFill];
        CGFloat textWidth = selfWidth - (kTextFieldMarginWidth * 2);
        CGFloat upperTextTop = kTextFieldMarginHeight;
        CGFloat underTextTop = upperTextTop + kTextFieldHeight + kSeparaorLineHeight + kTextFieldMarginHeight;
        self.upperTextField = [PJViewUtils generateTextFieldWithFrame:CGRectMake(kTextFieldMarginWidth, upperTextTop, textWidth, kTextFieldHeight)
                                                            superView:self
                                                                 text:@""
                                                            textColor:[UIColor blackColor]
                                                                 font:[UIFont boldSystemFontOfSize:kTextFieldFontSize]
                                                          placeHolder:@""
                                                             delegate:self
                                                             password:NO];
        self.upperTextField.borderStyle = UITextBorderStyleNone;
        self.underTextField = [PJViewUtils generateTextFieldWithFrame:CGRectMake(kTextFieldMarginWidth, underTextTop, textWidth, kTextFieldHeight)
                                                            superView:self
                                                                 text:@""
                                                            textColor:[UIColor blackColor]
                                                                 font:[UIFont boldSystemFontOfSize:kTextFieldFontSize]
                                                          placeHolder:@""
                                                             delegate:self
                                                             password:NO];
        self.underTextField.borderStyle = UITextBorderStyleNone;
        self.acceptKeyboard = NO;
        
    }
    return self;
}

-(void) setUpperText:(NSString *)upperText{
    self.upperTextField.text = upperText;
}

-(NSString*) upperText{
    return self.upperTextField.text;
}

-(void) setUpperTextColor:(UIColor *)upperTextColor{
    self.upperTextField.textColor = upperTextColor;
}

-(UIColor*) upperTextColor{
    return self.upperTextField.textColor;
}

-(void) setUpperPlaceHolder:(NSString *)upperPlaceHolder{
    self.upperTextField.placeholder = upperPlaceHolder;
}

-(NSString*) upperPlaceHolder{
    return self.upperTextField.placeholder;
}

-(void) setUpperPassword:(BOOL)upperPassword{
    self.upperTextField.secureTextEntry = upperPassword;
}

-(BOOL) upperPassword{
    return self.upperTextField.secureTextEntry;
}

-(void) setUnderText:(NSString *)underText{
    self.underTextField.text = underText;
}

-(NSString*) underText{
    return self.underTextField.text;
}

-(void) setUnderTextColor:(UIColor *)underTextColor{
    self.underTextField.textColor = underTextColor;
}

-(UIColor*) underTextColor{
    return self.underTextField.textColor;
}

-(void) setUnderPlaceHolder:(NSString *)underPlaceHolder{
    self.underTextField.placeholder = underPlaceHolder;
}

-(NSString*) underPlaceHolder{
    return self.underTextField.placeholder;
}

-(void) setUnderPassword:(BOOL)underPassword{
    self.underTextField.secureTextEntry = underPassword;
}

-(BOOL) underPassword{
    return self.underTextField.secureTextEntry;
}

-(BOOL) isMatchDoubleText{
    return [self.upperText isEqualToString:self.underText];
}

-(void) changeFoucusTextField:(UITextField*) textField{
    NSString* changeImageName = kDoubleBoxImageName;
    if (textField == self.upperTextField) {
        changeImageName = kDoubleBoxUpperImageName;
    } else if(textField == self.underTextField){
        changeImageName = kDoubleBoxUnderImageName;
    }
    self.backImageView.image = [UIImage imageNamed:changeImageName];
}

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    [self changeFoucusTextField:textField];
    return self.acceptKeyboard;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    return YES;
}

-(void) textFieldDidBeginEditing:(UITextField *)textField{
    [self changeFoucusTextField:textField];
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    self.backImageView.image = [UIImage imageNamed:kDoubleBoxImageName];
}


@end
