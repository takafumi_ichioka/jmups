//
//  PJBoxTextField.m
//  JMups
//
//  Created by flight on 2014/01/31.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBoxTextField.h"

#define kBoxTextImageName           @"box_text.png"
#define kBoxTextActiveImageName     @"box_text_active.png"

#define kTextFieldMarginWidth 4
#define kTextFieldDefaultFontSize 14

@interface PJBoxTextField ()<UITextFieldDelegate>

@property(nonatomic,strong) UITextField* boxTextField;
@property(nonatomic,strong) UIImageView* boxBackImageView;

@end

@implementation PJBoxTextField

+(UIImage*) boxTextFieldImage{
    return [UIImage imageNamed:kBoxTextImageName];
}
+(UIImage*) boxTextFieldActiveImage{
    return [UIImage imageNamed:kBoxTextActiveImageName];
}

-(id) initWithOrigin:(CGPoint)origin superView:(UIView *)view{
    UIImage* boxImage = [PJBoxTextField boxTextFieldImage];
    CGFloat selfWidth = boxImage.size.width;
    CGFloat selfHeight = boxImage.size.height;
    CGRect frame = CGRectMake(origin.x, origin.y,selfWidth , selfHeight);
    self = [super initWithFrame:frame superView:view];
    if (self) {
        self.boxBackImageView = [PJViewUtils generateImageViewWithImage:boxImage
                                                                  frame:CGRectMake(0, 0, selfWidth, selfHeight)
                                                              superView:self
                                                            contentMode:UIViewContentModeScaleToFill];
        self.boxTextField = [PJViewUtils generateTextFieldWithFrame:CGRectMake(kTextFieldMarginWidth, 0, selfWidth - (kTextFieldMarginWidth * 2), selfHeight)
                                                          superView:self
                                                               text:@""
                                                          textColor:[UIColor blackColor]
                                                               font:[UIFont systemFontOfSize:kTextFieldDefaultFontSize]
                                                        placeHolder:@""
                                                           delegate:self
                                                           password:NO];
        self.boxTextField.borderStyle = UITextBorderStyleNone;
    }
    return self;
}

-(UITextField*) textField{
    return self.boxTextField;
}

-(void) setActiveMode:(BOOL)activeMode{
    _activeMode = activeMode;
    self.boxBackImageView.image = activeMode ? [PJBoxTextField boxTextFieldActiveImage] : [PJBoxTextField boxTextFieldImage];
}

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    self.activeMode = YES;
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    return YES;
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    self.activeMode = NO;
}

@end
