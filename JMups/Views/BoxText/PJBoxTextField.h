//
//  PJBoxTextField.h
//  JMups
//
//  Created by flight on 2014/01/31.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"

@interface PJBoxTextField : PJBaseCustomView

@property(nonatomic,readonly) UITextField* textField;
@property(nonatomic)          BOOL         activeMode;

+(UIImage*) boxTextFieldImage;

-(id) initWithOrigin:(CGPoint)origin superView:(UIView *)view;

@end
