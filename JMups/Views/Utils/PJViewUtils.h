//
//  PJViewUtils.h
//  JMups
//
//  Created by flight on 2014/01/28.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PJViewUtils : NSObject

+(UIView*) generateViewWithFrame:(CGRect) frame
                       superView:(UIView*) view
                 backgroundColor:(UIColor*) backgroundColor;

+(UILabel*) generateLabelWithFrame:(CGRect) frame
                         superView:(UIView*) view
                              text:(NSString*) text
                         textColor:(UIColor*) textColor
                     textAlignment:(NSTextAlignment) alignment
                              font:(UIFont*) font
                     linebreakMode:(NSLineBreakMode) linebreakMode
                     numberOfLines:(NSInteger) numberOfLines;

+(UITextField*) generateTextFieldWithFrame:(CGRect) frame
                                 superView:(UIView*) view
                                      text:(NSString*) text
                                 textColor:(UIColor*) textColor
                                      font:(UIFont*) font
                               placeHolder:(NSString*) placeHolder
                                  delegate:(id<UITextFieldDelegate>) delegate
                                  password:(BOOL) password;

+(UIButton*) generateButtonWithType:(UIButtonType) type
                              frame:(CGRect) frame
                          superView:(UIView*) view
                              title:(NSString*) title
                         titleColor:(UIColor*) titleColor
                               font:(UIFont*) font
                              image:(UIImage*) image
                   isBackGoundImage:(BOOL) isBackground
                             target:(id) target
                           selector:(SEL) selector;

+(UIImageView*) generateImageViewWithImage:(UIImage*) image
                                     frame:(CGRect) frame
                                 superView:(UIView*) view
                               contentMode:(UIViewContentMode) contentMode;

+(UITableView*) generateTableViewWithStyle:(UITableViewStyle) style
                                    frame:(CGRect) frame
                                superView:(UIView*) view
                                 delegate:(id<UITableViewDelegate>) delegate
                               datasource:(id<UITableViewDataSource>) datasource
                                  bounces:(BOOL) bounces;

+(UIActivityIndicatorView*) generateIndicatorWithStyle:(UIActivityIndicatorViewStyle) style
                                             superView:(UIView*) view
                                                center:(CGPoint) center
                                              starting:(BOOL) starting;


+(UIProgressView*) genetateProgressViewWithStyle:(UIProgressViewStyle) style
                                           frame:(CGRect) frame
                                       superView:(UIView*) view
                                        progress:(CGFloat) progress;

+(void) superView:(UIView*) superView addSubView:(UIView*) view;

+(UIInterfaceOrientation) deviceInterfaceOrientation;

+(CGRect) deviceStatusBarFrame;

+(CGRect) frameController:(UIViewController*) controller
          withOrientation:(UIInterfaceOrientation) orientation
               isRotating:(BOOL) isRotating;

+(UIAlertView*) showAlertTitle:(NSString*) title
                       message:(NSString*) message
                   buttonTitle:(NSString*) buttonTitle
                      delegate:(id<UIAlertViewDelegate>) delegate;

+(UIAlertView*) showAlertConfirmTitle:(NSString*) title
                              message:(NSString*) message
                          cancelTitle:(NSString*) cancelTitle
                           otherTitle:(NSString*) otherTitle
                             delegate:(id<UIAlertViewDelegate>) delegate;

@end
