//
//  PJViewUtils.m
//  JMups
//
//  Created by flight on 2014/01/28.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJViewUtils.h"

@implementation PJViewUtils

+(UIView*) generateViewWithFrame:(CGRect) frame
                       superView:(UIView*) view
                 backgroundColor:(UIColor*) backgroundColor{
    UIView* generateView = [[UIView alloc] initWithFrame:frame];
    generateView.backgroundColor = backgroundColor;
    [self superView:view addSubView:generateView];
    return generateView;
}

+(UILabel*) generateLabelWithFrame:(CGRect) frame
                         superView:(UIView*) view
                              text:(NSString*) text
                         textColor:(UIColor*) textColor
                     textAlignment:(NSTextAlignment) alignment
                              font:(UIFont*) font
                     linebreakMode:(NSLineBreakMode) linebreakMode
                     numberOfLines:(NSInteger) numberOfLines{
    UILabel* label = [[UILabel alloc] initWithFrame:frame];
    label.text = text;
    label.textColor = textColor;
    label.textAlignment = alignment;
    label.font = font;
    label.lineBreakMode = linebreakMode;
    label.numberOfLines = numberOfLines;
    [self superView:view addSubView:label];
    return label;
}

+(UITextField*) generateTextFieldWithFrame:(CGRect) frame
                                 superView:(UIView*) view
                                      text:(NSString*) text
                                 textColor:(UIColor*) textColor
                                      font:(UIFont*) font
                               placeHolder:(NSString*) placeHolder
                                  delegate:(id<UITextFieldDelegate>) delegate
                                  password:(BOOL) password{
    UITextField* textField = [[UITextField alloc] initWithFrame:frame];
    textField.text = text;
    textField.textColor = textColor;
    textField.textAlignment = NSTextAlignmentLeft;
    textField.font = font;
    textField.placeholder = placeHolder;
    textField.delegate = delegate;
    textField.secureTextEntry = password;
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self superView:view addSubView:textField];
    return textField;
}

+(UIButton*) generateButtonWithType:(UIButtonType) type
                              frame:(CGRect) frame
                          superView:(UIView*) view
                              title:(NSString*) title
                         titleColor:(UIColor*) titleColor
                               font:(UIFont*) font
                              image:(UIImage*) image
                   isBackGoundImage:(BOOL) isBackground
                             target:(id) target
                           selector:(SEL) selector{
    UIButton* button = [UIButton buttonWithType:type];
    [button setFrame:frame];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    [button.titleLabel setFont:font];
    if (image) {
        if (isBackground) {
            [button setBackgroundImage:image forState:UIControlStateNormal];
        } else {
            [button setImage:image forState:UIControlStateNormal];
        }
    }
    if (target && selector && [target respondsToSelector:selector]) {
        [button addTarget:target
                   action:selector
         forControlEvents:UIControlEventTouchUpInside];
    }
    [self superView:view addSubView:button];
    return button;
}

+(UIImageView*) generateImageViewWithImage:(UIImage*) image
                                     frame:(CGRect) frame
                                 superView:(UIView*) view
                               contentMode:(UIViewContentMode) contentMode{
    UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = frame;
    imageView.contentMode = contentMode;
    [self superView:view addSubView:imageView];
    return imageView;
}

+(UITableView*) generateTableViewWithStyle:(UITableViewStyle) style
                                     frame:(CGRect) frame
                                 superView:(UIView*) view
                                  delegate:(id<UITableViewDelegate>) delegate
                                datasource:(id<UITableViewDataSource>) datasource
                                   bounces:(BOOL) bounces{
    UITableView* tableView = [[UITableView alloc] initWithFrame:frame style:style];
    tableView.delegate = delegate;
    tableView.dataSource = datasource;
    tableView.bounces = bounces;
    [self superView:view addSubView:tableView];
    return tableView;
}

+(UIActivityIndicatorView*) generateIndicatorWithStyle:(UIActivityIndicatorViewStyle) style
                                             superView:(UIView*) view
                                                center:(CGPoint) center
                                              starting:(BOOL) starting{
    UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    indicator.center = center;
    [self superView:view addSubView:indicator];
    if (starting) {
        [indicator startAnimating];
    }
    return indicator;
}

+(UIProgressView*) genetateProgressViewWithStyle:(UIProgressViewStyle) style
                                           frame:(CGRect) frame
                                       superView:(UIView*) view
                                        progress:(CGFloat) progress{
    UIProgressView* progressView = [[UIProgressView alloc] initWithFrame:frame];
    progressView.progressViewStyle = style;
    progressView.progress = progress;
    [self superView:view addSubView:progressView];
    return progressView;
}

+(void) superView:(UIView*) superView addSubView:(UIView*) view{
    if (superView && view) {
        [superView addSubview:view];
    }
}

+(UIInterfaceOrientation) deviceInterfaceOrientation{
    return [UIApplication sharedApplication].statusBarOrientation;
}

+(CGRect) deviceStatusBarFrame{
    CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
    CGRect deviceStatusBarFrame;
    if (UIInterfaceOrientationIsPortrait([self deviceInterfaceOrientation])) {
        deviceStatusBarFrame = statusBarFrame;
    } else {
        deviceStatusBarFrame = CGRectMake(CGRectGetMinY(statusBarFrame), CGRectGetMinX(statusBarFrame), CGRectGetHeight(statusBarFrame), CGRectGetWidth(statusBarFrame));
    }
    return deviceStatusBarFrame;
}

+(CGRect) frameController:(UIViewController*) controller
          withOrientation:(UIInterfaceOrientation) orientation
               isRotating:(BOOL)isRotating{
    CGFloat statusBarHeight = CGRectGetHeight([self deviceStatusBarFrame]);
    CGFloat controllerViewFrameWidth = CGRectGetWidth(controller.view.frame);
    CGFloat controllerViewFrameHeight = CGRectGetHeight(controller.view.frame);
    CGRect frame;
    if (UIInterfaceOrientationIsPortrait(orientation) && !isRotating) {
        frame = CGRectMake(0, statusBarHeight, controllerViewFrameWidth, controllerViewFrameHeight - statusBarHeight);
    } else {
        frame = CGRectMake(0, statusBarHeight, controllerViewFrameHeight, controllerViewFrameWidth - statusBarHeight);
    }
    return frame;
}

+(UIAlertView*) showAlertTitle:(NSString*) title
                       message:(NSString*) message
                   buttonTitle:(NSString*) buttonTitle
                      delegate:(id<UIAlertViewDelegate>) delegate{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:delegate
                                          cancelButtonTitle:buttonTitle
                                          otherButtonTitles:nil];
    [alert show];
    return alert;
}

+(UIAlertView*) showAlertConfirmTitle:(NSString*) title
                              message:(NSString*) message
                          cancelTitle:(NSString*) cancelTitle
                           otherTitle:(NSString*) otherTitle
                             delegate:(id<UIAlertViewDelegate>) delegate{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:delegate
                                          cancelButtonTitle:cancelTitle
                                          otherButtonTitles:otherTitle,nil];
    [alert show];
    return alert;
}

@end
