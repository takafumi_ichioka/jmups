//
//  PJLicenseCertificationView.h
//  JMups
//
//  Created by flight on 2014/01/31.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"

@protocol PJLicenseCertificationViewDelegate;

@interface PJLicenseCertificationView : PJBaseCustomView

@property(nonatomic,assign) id<PJLicenseCertificationViewDelegate> delegate;
-(id) initWithController:(UIViewController*) controller title:(NSString*) title;

@end

@protocol PJLicenseCertificationViewDelegate <NSObject>

@required
-(void) didExecuteAuth:(PJLicenseCertificationView*) view licenseKey:(NSString*) licenseKey;

@end
