//
//  PJLicenseCertificationView.m
//  JMups
//
//  Created by flight on 2014/01/31.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJLicenseCertificationView.h"
#import "PJTabBackImageView.h"
#import "NSString+JMups.h"

#define kAuthButtonImageName        @"btn_auth.png"
#define kAuthButtonActiveImageName  @"btn_auth_active.png"

#define kViewMarginWidth                15
#define kViewMarginHeight               15
#define kItemLabelHeight                24
#define kItemLabelFontSize              14
#define kItemLabelMarginHeight          5
#define kLicenseKeyTextFieldCount       4
#define kLicenseKeyTextFieldMarginWidth 10
#define kLicenseKeyTextFieldHeight      40
#define kLicenseKeyTextFieldFontSize    14
#define kAuthButtonTopMarginHeight      50

@interface PJLicenseCertificationView ()

@property(nonatomic,copy) NSArray* licenseKeyTextFields;

@end

@implementation PJLicenseCertificationView

-(id) initWithController:(UIViewController*) controller title:(NSString *)title{
    CGSize imageSize = [PJTabBackImageView backIamgeViewType:PJTabBackImageTypeWide].size;
    CGFloat selfY = CGRectGetHeight([PJViewUtils deviceStatusBarFrame]);
    CGRect frame = CGRectMake(0, selfY, imageSize.width, imageSize.height);
    self = [super initWithFrame:frame superView:controller.view];
    if (self) {
        PJTabBackImageView* backImageView = [[PJTabBackImageView alloc] initWithOrigin:CGPointZero
                                                                             superView:self
                                                                             imageType:PJTabBackImageTypeWide
                                                                                 title:title];
        
        CGFloat labelTop = kViewMarginHeight + [backImageView backImageTabBarHeight];
        CGFloat viewWidth = imageSize.width - (kViewMarginWidth * 2);
        [PJViewUtils generateLabelWithFrame:CGRectMake(kViewMarginWidth, labelTop, viewWidth, kItemLabelHeight)
                                  superView:backImageView
                                       text:[NSString localizedStringForKey:@"licenseKeyLabel"]
                                  textColor:[UIColor blackColor]
                              textAlignment:NSTextAlignmentLeft
                                       font:[UIFont boldSystemFontOfSize:kItemLabelFontSize]
                              linebreakMode:NSLineBreakByClipping
                              numberOfLines:1];
        CGFloat textFieldTop = labelTop + kItemLabelHeight + kItemLabelMarginHeight;
        CGFloat textFieldWidth = (viewWidth / kLicenseKeyTextFieldCount) -  kLicenseKeyTextFieldMarginWidth;
        NSMutableArray* tmp = @[].mutableCopy;
        for (int i = 0; i < kLicenseKeyTextFieldCount; i++) {
            CGFloat textFieldX = kViewMarginWidth + ((textFieldWidth + kLicenseKeyTextFieldMarginWidth) * i);
            UITextField* textField = [PJViewUtils generateTextFieldWithFrame:CGRectMake(textFieldX, textFieldTop, textFieldWidth, kLicenseKeyTextFieldHeight)
                                                                   superView:backImageView
                                                                        text:@""
                                                                   textColor:[UIColor blackColor]
                                                                        font:[UIFont boldSystemFontOfSize:kLicenseKeyTextFieldFontSize]
                                                                 placeHolder:@""
                                                                    delegate:nil
                                                                    password:NO];
            [tmp addObject:textField];
        }
        self.licenseKeyTextFields = tmp.copy;
        CGFloat buttonTop = textFieldTop + kLicenseKeyTextFieldHeight + kAuthButtonTopMarginHeight;
        UIImage* buttonImage = [UIImage imageNamed:kAuthButtonImageName];
        CGSize buttonSize = buttonImage.size;
        CGFloat buttonX = imageSize.width - kViewMarginWidth - buttonImage.size.width;
        UIButton* authButton = [PJViewUtils generateButtonWithType:UIButtonTypeCustom
                                                             frame:CGRectMake(buttonX, buttonTop, buttonSize.width, buttonSize.height)
                                                         superView:backImageView
                                                             title:@""
                                                        titleColor:nil
                                                              font:nil
                                                             image:buttonImage
                                                  isBackGoundImage:NO
                                                            target:self
                                                          selector:@selector(pushedAuthButton:)];
        [authButton setImage:[UIImage imageNamed:kAuthButtonActiveImageName] forState:UIControlStateHighlighted];
        
    }
    return self;
}

-(void) pushedAuthButton:(UIButton*) button{
    if (_delegate && [_delegate respondsToSelector:@selector(didExecuteAuth:licenseKey:)]) {
        [_delegate didExecuteAuth:self licenseKey:nil];
        //        [_delegate didExecuteAuth:self userInfo:@{PJInitCertificationActivateSequnceNumber:self.sequenceTextField.text,
        //                                                  PJInitCertificationActivateId:self.activateIdTextField.text,
        //                                                  PJInitCertificationActivateOneTimePassword:self.onetimePasswordTextField.text,
        //                                                  PJInitCertificationAppliPassword:self.passwordDoubleTextField.upperText}];
    }
}

@end
