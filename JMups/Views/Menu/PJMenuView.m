//
//  PJMenuView.m
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJMenuView.h"
#import "PJMenuCell.h"
#import "PJTabBackImageView.h"

static NSString* MenuCellIdentifier = @"MenuCellIdentifier";

@interface PJMenuView ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView* tableView;
@property(nonatomic,strong) NSArray*     menuList;
@property(nonatomic,strong) NSMutableArray* switchList;
@property(nonatomic,copy)   NSIndexPath*    selectedIndexPath;

@end

@implementation PJMenuView

-(id) initWithController:(UIViewController*) controller
               withTitle:(NSString*) title
            withMenuList:(NSArray*) menuList;{
    UIImage* backGroundImage = [PJTabBackImageView backIamgeViewType:PJTabBackImageTypeHalf];
    CGFloat frameWidth = backGroundImage.size.width;
    CGFloat frameHeight = backGroundImage.size.height;
    CGRect frame = CGRectMake(0, CGRectGetHeight([PJViewUtils deviceStatusBarFrame]), frameWidth, frameHeight);
    self = [super initWithFrame:frame superView:controller.view];
    if (self) {
        PJTabBackImageView* tabImageBackView = [[PJTabBackImageView alloc] initWithOrigin:CGPointZero
                                                                                superView:self
                                                                                imageType:PJTabBackImageTypeHalf
                                                                                    title:title];
        CGFloat tabBarHeight = [tabImageBackView backImageTabBarHeight];
        CGFloat tableWidth = [PJMenuCell cellSize].width;
        self.tableView = [PJViewUtils generateTableViewWithStyle:UITableViewStylePlain
                                                           frame:CGRectMake(0, tabBarHeight, tableWidth, frameHeight - tabBarHeight)
                                                       superView:tabImageBackView
                                                        delegate:self
                                                      datasource:self
                                                         bounces:NO];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.tableHeaderView = [[UIView alloc] init];
        self.tableView.tableFooterView = [[UIView alloc] init];
        [self.tableView registerClass:[PJMenuCell class] forCellReuseIdentifier:MenuCellIdentifier];
        self.menuList = menuList.copy;
        self.switchList = @[].mutableCopy;
        for (int i = 0; i < menuList.count;i++) {
            [self.switchList addObject:@(NO)];
        }
    }
    return self;
}

-(void) clearSelect{
    self.selectedIndexPath = nil;
    [self reload];
}

-(void) reload{
    [self.tableView reloadData];
}

-(void) selectedMenuInformation:(PJMenuInformation*) menuInformation{
    if (menuInformation.parent) {
        _selectedIndexPath = [NSIndexPath indexPathForRow:menuInformation.index + 1
                                                inSection:menuInformation.parent.index];
    } else {
        _selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:menuInformation.index];
    }
    [self reload];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return self.menuList.count;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rowCount = 1;
    if ([self.switchList[section] boolValue]) {
        PJMenuInformation* menuInformation = self.menuList[section];
        NSInteger childrenCount = menuInformation.children.count;
        if (childrenCount > 0) {
            rowCount += childrenCount;
        }
    }
    return rowCount;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PJMenuCell cellSize].height;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PJMenuCell* cell = [tableView dequeueReusableCellWithIdentifier:MenuCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[PJMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MenuCellIdentifier];
    }
    BOOL activeMode = (self.selectedIndexPath && [self.selectedIndexPath isEqual:indexPath]);
    PJMenuInformation* information = self.menuList[indexPath.section];
    if (indexPath.row == 0) {
        cell.text = information.text;
        cell.type = (information.children.count == 0) ? PJMenuCellTypeDefault : PJMenuCellTypeHeader;
    } else {
        PJMenuInformation* childInformation = information.children[indexPath.row - 1];
        cell.text = childInformation.text;
        cell.type = PJMenuCellTypeDefault;
    }
    cell.activeMode = activeMode;
    cell.enabled = (!self.selectedIndexPath || activeMode);
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    PJMenuInformation* information = self.menuList[indexPath.section];
    if (indexPath.row == 0 && information.children.count > 0) {
        BOOL switchValue = [self.switchList[indexPath.section] boolValue];
        self.switchList[indexPath.section] = @(!switchValue);
        NSIndexSet* indexSet = [NSIndexSet indexSetWithIndex:indexPath.section];
        [tableView reloadSections:indexSet
                 withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        self.selectedIndexPath = indexPath;
        if (self.selectedHandler) {
            PJMenuInformation* selectInformation;
            if (indexPath.row == 0) {
                selectInformation = information;
            } else {
                selectInformation = information.children[indexPath.row - 1];
            }
            self.selectedHandler(selectInformation);
        }
        [self reload];
    }
    
}

@end
