//
//  PJMenuCell.m
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJMenuCell.h"
#import "PJViewUtils.h"

#define kCellSizeWidth  255.0f
#define kCellSizeHeight 70.0f

#define kHeaderBackImageName    @"cell_header.png"
#define kDefalutBackImageName   @"cell_base.png"
#define kActiveBackImageName    @"cell_base_active.png"

#define kTextFontSize       24
#define kTextMarginWitdh    30

@interface PJMenuCell ()

@property(nonatomic,strong) UIImageView* backImageView;
@property(nonatomic,strong) UILabel*     textItemLabel;

@end

@implementation PJMenuCell

+(CGSize) cellSize{
    return CGSizeMake(kCellSizeWidth, kCellSizeHeight);
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        CGSize selfSize = [PJMenuCell cellSize];
        _backImageView = [PJViewUtils generateImageViewWithImage:[UIImage imageNamed:kDefalutBackImageName]
                                                           frame:CGRectMake(0, 0, selfSize.width, selfSize.height)
                                                       superView:self.contentView
                                                     contentMode:UIViewContentModeScaleToFill];
        _textItemLabel = [PJViewUtils generateLabelWithFrame:CGRectMake(kTextMarginWitdh, 0, selfSize.width - (kTextMarginWitdh * 2), selfSize.height)
                                                   superView:self.contentView
                                                        text:@""
                                                   textColor:[UIColor blackColor]
                                               textAlignment:NSTextAlignmentCenter
                                                        font:[UIFont boldSystemFontOfSize:kTextFontSize]
                                               linebreakMode:NSLineBreakByClipping
                                               numberOfLines:1];
        self.type = PJMenuCellTypeDefault;
        self.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    return self;
}

-(void) setText:(NSString *)text{
    _textItemLabel.text = text;
}

-(NSString*) text{
    return _textItemLabel.text;
}

-(void) setType:(PJMenuCellType)type{
    _type = type;
    [self setImageWithType:type activeMode:self.activeMode];
    
}

-(void) setActiveMode:(BOOL)activeMode{
    _activeMode = activeMode;
    _textItemLabel.textColor = activeMode ? [UIColor whiteColor] : [UIColor blackColor];
    [self setImageWithType:self.type activeMode:activeMode];
}

-(void) setEnabled:(BOOL)enabled{
    _enabled = enabled;
    self.userInteractionEnabled = enabled;
    self.contentView.alpha = enabled ? 1.0 : 0.4;
}

-(void) setImageWithType:(PJMenuCellType) type activeMode:(BOOL) activeMode{
    NSString* imageName = @"";
    if (type == PJMenuCellTypeDefault) {
        imageName = activeMode ? kActiveBackImageName : kDefalutBackImageName;
    } else {
        imageName = kHeaderBackImageName;
    }
    _backImageView.image = [UIImage imageNamed:imageName];
}

@end
