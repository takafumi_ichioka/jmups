//
//  PJMenuCell.h
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    PJMenuCellTypeHeader,
    PJMenuCellTypeDefault
} PJMenuCellType;

@interface PJMenuCell : UITableViewCell

@property(nonatomic,copy) NSString* text;
@property(nonatomic)      PJMenuCellType type;
@property(nonatomic)      BOOL activeMode;
@property(nonatomic)      BOOL enabled;

+(CGSize) cellSize;

@end
