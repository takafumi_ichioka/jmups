//
//  PJMenuView.h
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"
#import "PJMenuInformation.h"

typedef void(^PJMenuSelectedHandler)(PJMenuInformation* menuInfomation);

@interface PJMenuView : PJBaseCustomView

@property(nonatomic,copy) PJMenuSelectedHandler selectedHandler;
-(id) initWithController:(UIViewController*) controller
               withTitle:(NSString*) title
            withMenuList:(NSArray*) menuList;
-(void) selectedMenuInformation:(PJMenuInformation*) menuInformation;
-(void) clearSelect;
-(void) reload;

@end
