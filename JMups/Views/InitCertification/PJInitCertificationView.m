//
//  PJInitCertificationView.m
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJInitCertificationView.h"
#import "PJTabBackImageView.h"
#import "PJDoubleTextFieldView.h"
#import "PJBoxTextField.h"
#import "NSString+JMups.h"

#define kAuthButtonImageName        @"btn_auth.png"
#define kAuthButtonActiveImageName  @"btn_auth_active.png"

#define kViewMarginHeight           15
#define kItemLabelHeight            24
#define kItemLabelFontSize          14
#define kItemLabelMarginHeight      5
#define kAnnotationLabelHeight      60
#define kAnnotationLabelFontSize    18
#define kActivateTextFieldFontSize  14
#define kAuthButtonTopMarginHeight  20

@interface PJInitCertificationView ()

@property(nonatomic,strong) PJBoxTextField* sequenceTextField;
@property(nonatomic,strong) PJBoxTextField* activateIdTextField;
@property(nonatomic,strong) PJBoxTextField* onetimePasswordTextField;
@property(nonatomic,strong) PJDoubleTextFieldView* passwordDoubleTextField;

@end

@implementation PJInitCertificationView

-(id) initWithController:(UIViewController*) controller title:(NSString *)title{
    CGSize imageSize = [PJTabBackImageView backIamgeViewType:PJTabBackImageTypeLine].size;
    CGFloat selfY = CGRectGetHeight([PJViewUtils deviceStatusBarFrame]);
    CGRect frame = CGRectMake(0, selfY, imageSize.width, imageSize.height);
    self = [super initWithFrame:frame superView:controller.view];
    if (self) {
        PJTabBackImageView* backImageView = [[PJTabBackImageView alloc] initWithOrigin:CGPointZero
                                                                             superView:self
                                                                             imageType:PJTabBackImageTypeLine
                                                                                 title:title];
        
        CGFloat activateTop = kViewMarginHeight + [backImageView backImageTabBarHeight];
        CGSize doubleTextFieldImageSize = [PJDoubleTextFieldView doubleTextBackImage].size;
        CGFloat lineHalfWidth = [backImageView lineHalfWidth];
        CGFloat doubleTextFieldX = lineHalfWidth + ((lineHalfWidth - doubleTextFieldImageSize.width) / 2);
        CGFloat doubleTextTop = activateTop;
        [self generateActivateBoxTextField:self.sequenceTextField
                             backImageView:backImageView
                                   withTop:&activateTop
                                  withText:@""
                           withPlaceHolder:[NSString localizedStringForKey:@"activateSequencePlacehodler"]
                             withLabelText:[NSString localizedStringForKey:@"activateSequenceTitle"]];
        [self generateActivateBoxTextField:self.activateIdTextField
                             backImageView:backImageView
                                   withTop:&activateTop
                                  withText:@""
                           withPlaceHolder:[NSString localizedStringForKey:@"activateIdPlacehodler"]
                             withLabelText:[NSString localizedStringForKey:@"activateIdTitle"]];
        [self generateActivateBoxTextField:self.onetimePasswordTextField
                             backImageView:backImageView
                                   withTop:&activateTop
                                  withText:@""
                           withPlaceHolder:[NSString localizedStringForKey:@"activatePasswordPlacehodler"]
                             withLabelText:[NSString localizedStringForKey:@"activatePasswordTitle"]];
        
        [PJViewUtils generateLabelWithFrame:CGRectMake(doubleTextFieldX,doubleTextTop, doubleTextFieldImageSize.width, kItemLabelHeight)
                                  superView:backImageView
                                       text:[NSString localizedStringForKey:@"passwordTitle"]
                                  textColor:[UIColor blackColor]
                              textAlignment:NSTextAlignmentLeft
                                       font:[UIFont boldSystemFontOfSize:kItemLabelFontSize]
                              linebreakMode:NSLineBreakByClipping
                              numberOfLines:1];
        doubleTextTop += (kItemLabelHeight + kItemLabelMarginHeight);
        self.passwordDoubleTextField = [[PJDoubleTextFieldView alloc] initWithOrigin:CGPointMake(doubleTextFieldX, doubleTextTop)
                                                                           superView:backImageView];
        self.passwordDoubleTextField.upperPlaceHolder = [NSString localizedStringForKey:@"passwordUpperPlaceHolder"];
        self.passwordDoubleTextField.upperPassword = YES;
        self.passwordDoubleTextField.underPlaceHolder = [NSString localizedStringForKey:@"passwordUnderPlaceHolder"];
        self.passwordDoubleTextField.underPassword = YES;
        self.passwordDoubleTextField.acceptKeyboard = YES;
        doubleTextTop += (doubleTextFieldImageSize.height + kViewMarginHeight);
        [PJViewUtils generateLabelWithFrame:CGRectMake(doubleTextFieldX,doubleTextTop, doubleTextFieldImageSize.width, kAnnotationLabelHeight)
                                  superView:backImageView
                                       text:[NSString localizedStringForKey:@"passwordAnnotaion"]
                                  textColor:[UIColor blackColor]
                              textAlignment:NSTextAlignmentLeft
                                       font:[UIFont boldSystemFontOfSize:kAnnotationLabelFontSize]
                              linebreakMode:NSLineBreakByWordWrapping
                              numberOfLines:2];
        CGFloat buttonTop = backImageView.lineButtom + kAuthButtonTopMarginHeight;
        UIImage* buttonImage = [UIImage imageNamed:kAuthButtonImageName];
        CGSize buttonSize = buttonImage.size;
        CGFloat buttonX = doubleTextFieldX + ((lineHalfWidth - buttonSize.width) / 2);
        UIButton* authButton = [PJViewUtils generateButtonWithType:UIButtonTypeCustom
                                                             frame:CGRectMake(buttonX, buttonTop, buttonSize.width, buttonSize.height)
                                                         superView:backImageView
                                                             title:@""
                                                        titleColor:nil
                                                              font:nil
                                                             image:buttonImage
                                                  isBackGoundImage:NO
                                                            target:self
                                                          selector:@selector(pushedAuthButton:)];
        [authButton setImage:[UIImage imageNamed:kAuthButtonActiveImageName] forState:UIControlStateHighlighted];
        
    }
    return self;
}

-(void) generateActivateBoxTextField:(PJBoxTextField*) boxTextField
                       backImageView:(PJTabBackImageView*) backImageView
                             withTop:(CGFloat*) top
                            withText:(NSString*) text
                     withPlaceHolder:(NSString*) plcaceHolder
                       withLabelText:(NSString*) labelText{
    
    CGFloat lineHalfWidth = [backImageView lineHalfWidth];
    CGSize boxTextSize = [PJBoxTextField boxTextFieldImage].size;
    CGFloat activateViewMarginWidth = ((lineHalfWidth - boxTextSize.width) / 2);
    [PJViewUtils generateLabelWithFrame:CGRectMake(activateViewMarginWidth, *top, boxTextSize.width, kItemLabelHeight)
                              superView:backImageView
                                   text:labelText
                              textColor:[UIColor blackColor]
                          textAlignment:NSTextAlignmentLeft
                                   font:[UIFont boldSystemFontOfSize:kItemLabelFontSize]
                          linebreakMode:NSLineBreakByClipping
                          numberOfLines:1];
    *top += (kItemLabelHeight + kItemLabelMarginHeight);
    boxTextField = [[PJBoxTextField alloc] initWithOrigin:CGPointMake(activateViewMarginWidth, *top) superView:backImageView];
    boxTextField.textField.text = text;
    boxTextField.textField.font = [UIFont boldSystemFontOfSize:kActivateTextFieldFontSize];
    boxTextField.textField.placeholder = plcaceHolder;
    *top += (boxTextSize.height + kViewMarginHeight);
}

-(void) pushedAuthButton:(UIButton*) button{
    if (_delegate && [_delegate respondsToSelector:@selector(didExecuteAuth:userInfo:)]) {
        [_delegate didExecuteAuth:self userInfo:nil];
//        [_delegate didExecuteAuth:self userInfo:@{PJInitCertificationActivateSequnceNumber:self.sequenceTextField.text,
//                                                  PJInitCertificationActivateId:self.activateIdTextField.text,
//                                                  PJInitCertificationActivateOneTimePassword:self.onetimePasswordTextField.text,
//                                                  PJInitCertificationAppliPassword:self.passwordDoubleTextField.upperText}];
    }
}

@end
