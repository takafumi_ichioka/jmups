//
//  PJInitCertificationView.h
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"

static const NSString* PJInitCertificationActivateSequnceNumber    = @"PJInitCertificationActivateSequnceNumber";
static const NSString* PJInitCertificationActivateId               = @"PJInitCertificationActivateId";
static const NSString* PJInitCertificationActivateOneTimePassword  = @"PJInitCertificationActivateOneTimePassword";
static const NSString* PJInitCertificationAppliPassword            = @"PJInitCertificationAppliPassword";

@protocol PJInitCertificationViewDelegate;

@interface PJInitCertificationView : PJBaseCustomView

@property(nonatomic,assign) id<PJInitCertificationViewDelegate> delegate;
-(id) initWithController:(UIViewController*) controller title:(NSString*) title;

@end

@protocol PJInitCertificationViewDelegate <NSObject>

@required
-(void) didExecuteAuth:(PJInitCertificationView*) view userInfo:(NSDictionary*) userInfo;

@end
