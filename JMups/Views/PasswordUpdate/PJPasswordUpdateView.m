//
//  PJPasswordUpdateView.m
//  JMups
//
//  Created by flight on 2014/01/31.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJPasswordUpdateView.h"
#import "PJTabBackImageView.h"
#import "PJDoubleTextFieldView.h"
#import "NSString+JMups.h"

#define kSaveButtonImageName            @"btn_save.png"
#define kSaveButtonActiveImageName      @"btn_save_ative.png"
#define kCancelButtonImageName          @"btn_cancel.png"
#define kCancelButtonActiveImageName    @"btn_cancel_active.png"

#define kViewMarginHeight               15
#define kItemLabelHeight                30
#define kItemLabelFontSize              16
#define kItemLabelMarginHeight          5
#define kSaveButtonTopMarinHeight       200
#define kCancelButtonTopMarinHeight     10

@interface PJPasswordUpdateView ()

@property(nonatomic,strong) PJDoubleTextFieldView* doubleTextField;

@end

@implementation PJPasswordUpdateView

-(id) initWithController:(UIViewController*) controller title:(NSString *)title{
    CGSize imageSize = [PJTabBackImageView backIamgeViewType:PJTabBackImageTypeHalf].size;
    CGFloat selfY = CGRectGetHeight([PJViewUtils deviceStatusBarFrame]);
    CGRect frame = CGRectMake(0, selfY, imageSize.width, imageSize.height);
    self = [super initWithFrame:frame superView:controller.view];
    if (self) {
        PJTabBackImageView* backImageView = [[PJTabBackImageView alloc] initWithOrigin:CGPointZero
                                                                             superView:self
                                                                             imageType:PJTabBackImageTypeHalf
                                                                                 title:title];
        
        CGSize doubleTextFieldSize = [PJDoubleTextFieldView doubleTextBackImage].size;
        CGFloat labelTop = kViewMarginHeight + [backImageView backImageTabBarHeight];
        CGFloat viewMarginWidth = (imageSize.width - doubleTextFieldSize.width) /2;
        [PJViewUtils generateLabelWithFrame:CGRectMake(viewMarginWidth, labelTop, doubleTextFieldSize.width, kItemLabelHeight)
                                  superView:backImageView
                                       text:[NSString localizedStringForKey:@"passwordUpdateLabel"]
                                  textColor:[UIColor blackColor]
                              textAlignment:NSTextAlignmentLeft
                                       font:[UIFont boldSystemFontOfSize:kItemLabelFontSize]
                              linebreakMode:NSLineBreakByClipping
                              numberOfLines:1];
        CGFloat doubleTextFieldTop = labelTop + kItemLabelHeight + kItemLabelMarginHeight;
        self.doubleTextField = [[PJDoubleTextFieldView alloc] initWithOrigin:CGPointMake(viewMarginWidth, doubleTextFieldTop)
                                                                   superView:backImageView];
        
        CGFloat saveButtonTop = doubleTextFieldTop + kSaveButtonTopMarinHeight + doubleTextFieldSize.height;
        UIImage* saveButtonImage = [UIImage imageNamed:kSaveButtonImageName];
        CGSize saveButtonSize = saveButtonImage.size;
        CGFloat saveButtonMarginWidth = (imageSize.width -  saveButtonSize.width) / 2;
        UIButton* saveButton = [PJViewUtils generateButtonWithType:UIButtonTypeCustom
                                                             frame:CGRectMake(saveButtonMarginWidth, saveButtonTop, saveButtonSize.width, saveButtonSize.height)
                                                         superView:backImageView
                                                             title:@""
                                                        titleColor:nil
                                                              font:nil
                                                             image:saveButtonImage
                                                  isBackGoundImage:NO
                                                            target:self
                                                          selector:@selector(pushedSaveButton:)];
        [saveButton setImage:[UIImage imageNamed:kSaveButtonActiveImageName] forState:UIControlStateHighlighted];
        
        CGFloat buttonTop = doubleTextFieldTop + kSaveButtonTopMarinHeight + doubleTextFieldSize.height;
        buttonTop = [self nextButtonTopWithButtonImage:[UIImage imageNamed:kSaveButtonImageName]
                                     activeButtonImage:[UIImage imageNamed:kSaveButtonActiveImageName]
                                         backImageView:backImageView
                                             buttonTop:buttonTop
                                             selfWidth:imageSize.width
                                              selector:@selector(pushedSaveButton:)];
        buttonTop += kCancelButtonTopMarinHeight;
        buttonTop = [self nextButtonTopWithButtonImage:[UIImage imageNamed:kCancelButtonImageName]
                                     activeButtonImage:[UIImage imageNamed:kCancelButtonActiveImageName]
                                         backImageView:backImageView
                                             buttonTop:buttonTop
                                             selfWidth:imageSize.width
                                              selector:@selector(pushedCancelButton:)];
    }
    return self;
}

-(CGFloat) nextButtonTopWithButtonImage:(UIImage*) buttonImage
                      activeButtonImage:(UIImage*) activeButtonImage
                          backImageView:(PJTabBackImageView*) backImageView
                              buttonTop:(CGFloat) buttonTop
                              selfWidth:(CGFloat) selfWidth
                               selector:(SEL) selector{
    CGSize buttonSize = buttonImage.size;
    CGFloat buttonMarginWidth = (selfWidth -  buttonSize.width) / 2;
    UIButton* button = [PJViewUtils generateButtonWithType:UIButtonTypeCustom
                                                         frame:CGRectMake(buttonMarginWidth, buttonTop, buttonSize.width, buttonSize.height)
                                                     superView:backImageView
                                                         title:@""
                                                    titleColor:nil
                                                          font:nil
                                                         image:buttonImage
                                              isBackGoundImage:NO
                                                        target:self
                                                      selector:selector];
    [button setImage:activeButtonImage forState:UIControlStateHighlighted];
    return buttonTop + buttonSize.height;
}

-(void) pushedSaveButton:(UIButton*) button{
    if (_delegate && [_delegate respondsToSelector:@selector(didCompletedUpdatePassword:)]) {
        [_delegate didCompletedUpdatePassword:self];
    }
}


-(void) pushedCancelButton:(UIButton*) button{
    if (_delegate && [_delegate respondsToSelector:@selector(didCancelUpdatePassword:)]) {
        [_delegate didCancelUpdatePassword:self];
    }
}
@end
