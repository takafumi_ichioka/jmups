//
//  PJPasswordUpdateView.h
//  JMups
//
//  Created by flight on 2014/01/31.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"

@protocol PJPasswordUpdateViewDelegate;

@interface PJPasswordUpdateView : PJBaseCustomView

@property(nonatomic,assign) id<PJPasswordUpdateViewDelegate> delegate;
-(id) initWithController:(UIViewController*) controller title:(NSString*) title;

@end

@protocol PJPasswordUpdateViewDelegate <NSObject>

@required
-(void) didCompletedUpdatePassword:(PJPasswordUpdateView*) view;
-(void) didCancelUpdatePassword:(PJPasswordUpdateView*) view;

@end