//
//  PJLoginView.m
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJLoginView.h"
#import "UIImage+JMups.h"
#import "PJKeyPadUtils.h"

#define kLoginBackgounndImageName @"bg_login"
#define kLoginViewMarginWidth 15
#define kInputCount 4
#define kInputImageViewTop 148
#define kInputImageSize     36
#define kInputImageViewWidth 62
#define kInputImageViewHeight 53
#define kInputImageMarginWidth 11
#define kKeyPadWidthCount 3
#define kKeyPadHeightCount 4
#define kKeyPadImageWidth 86
#define kKeyPadImageHeight 48
#define kKeyPadTopMarginHeight 8
#define kKeyPadButtomMarginHeight 16

#define kInputImageMinTag   100
#define kKeyPadButtonMinTag 200

@interface PJLoginView ()

@property(nonatomic,strong) NSArray* keyPadList;
@property(nonatomic,strong) NSString* password;

@end

@implementation PJLoginView

-(id) initWithOrigin:(CGPoint) orign SuperView:(UIView*) superView{
    UIImage* backImage = [UIImage imageNamed:kLoginBackgounndImageName];
    CGFloat selfWidth = backImage.size.width;
    CGFloat selfHeight = backImage.size.height;
    CGRect frame = CGRectMake(orign.x, orign.y, selfWidth, selfHeight);
    self = [super initWithFrame:frame superView:superView];
    if (self) {
        _password = @"";
        [PJViewUtils generateImageViewWithImage:backImage
                                          frame:frame
                                      superView:self
                                    contentMode:UIViewContentModeCenter];
        UIImage* circleImage = [UIImage circleImageSize:CGSizeMake(kInputImageSize, kInputImageSize)
                                            circleColor:[UIColor blackColor]];
        for (int i = 0; i < kInputCount; i++) {
            CGFloat circleImageX = kLoginViewMarginWidth + ((kInputImageViewWidth + kInputImageMarginWidth) * i);
            UIImageView* circleImageView = [PJViewUtils generateImageViewWithImage:circleImage
                                                                             frame:CGRectMake(circleImageX, kInputImageViewTop, kInputImageViewWidth, kInputImageViewHeight)
                                                                         superView:self
                                                                       contentMode:UIViewContentModeCenter];
            circleImageView.tag = i + kInputImageMinTag;
        }
        _keyPadList = @[@(PJKeyPadNumberOne),@(PJKeyPadNumberTwo),@(PJKeyPadNumberThree),@(PJKeyPadNumberFour),
                        @(PJKeyPadNumberFive),@(PJKeyPadNumberSix),@(PJKeyPadNumberSeven),@(PJKeyPadNumberEight),
                        @(PJKeyPadNumberNine),@(PJKeyPadNumberZero),@(PJKeyPadNumberBack)];
        CGFloat keyPadTop = kInputImageViewTop + kInputImageViewHeight + kKeyPadTopMarginHeight;
        CGFloat keyPadViewWidth = selfWidth - (kLoginViewMarginWidth * 2);
        CGFloat keyPadViewHeight = selfHeight - keyPadTop - kKeyPadButtomMarginHeight;
        CGFloat keyPadMaringWidth = (keyPadViewWidth - (kKeyPadImageWidth * kKeyPadWidthCount)) / (kKeyPadWidthCount - 1);
        CGFloat keyPadMaringHeight = (keyPadViewHeight - (kKeyPadImageHeight * kKeyPadHeightCount))/ (kKeyPadHeightCount - 1);
        int keyPadIndex = 0;
        for (int j = 0; j < kKeyPadHeightCount;j++) {
            for (int k = 0; k < kKeyPadWidthCount; k++) {
                if (j == kKeyPadHeightCount - 1 && k == 0) {
                    continue;
                }
                PJKeyPadNumber number = [_keyPadList[keyPadIndex] integerValue];
                UIImage* keyPadImage = [PJKeyPadUtils keyPadImageWithNumber:number onImage:NO];
                UIImage* keyPadOnImage = [PJKeyPadUtils keyPadImageWithNumber:number onImage:YES];
                CGFloat keyPadImageX = kLoginViewMarginWidth + ((kKeyPadImageWidth + keyPadMaringWidth) * k);
                CGFloat keyPadImageY = keyPadTop + ((kKeyPadImageHeight + keyPadMaringHeight) * j);
                UIButton* keyPadButton = [PJViewUtils generateButtonWithType:UIButtonTypeCustom
                                                                       frame:CGRectMake(keyPadImageX, keyPadImageY, kKeyPadImageWidth, kKeyPadImageHeight)
                                                                   superView:self
                                                                       title:nil
                                                                  titleColor:nil
                                                                        font:nil
                                                                       image:keyPadImage
                                                            isBackGoundImage:NO
                                                                      target:self
                                                                    selector:@selector(pushedKeyPadButton:)];
                [keyPadButton setImage:keyPadOnImage forState:UIControlStateHighlighted];
                keyPadButton.tag = keyPadIndex + kKeyPadButtonMinTag;
                keyPadIndex += 1;
            }
        }
    }
    return self;
}

-(void) pushedKeyPadButton:(UIButton*) button{
    NSInteger index = button.tag - kKeyPadButtonMinTag;
    PJKeyPadNumber number = [_keyPadList[index] integerValue];
    __weak PJLoginView* weakSelf = self;
    [PJKeyPadUtils tappedKeyPadNumber:number valueString:_password tappedHandler:^(NSString* editString,PJKeyPadNumber number){
        weakSelf.password = editString;
        PJLog(@"editString = %@",editString);
    }];
}



@end
