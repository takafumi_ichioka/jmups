//
//  PJLoginView.h
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"

@interface PJLoginView : PJBaseCustomView

-(id) initWithOrigin:(CGPoint) orign SuperView:(UIView*) superView;

@end
