//
//  PJTabHalfView.m
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJTabBackImageView.h"

#define kHalfBackGoundImageName @"bg_tab_helf.png"
#define kLineBackGoundImageName @"bg_tab_line.png"
#define kWideBackGoundImageName @"bg_tab_wide.png"

#define kHalfLineWidth 254.0
#define kLineButtom    480
#define kTabBarHeight 44.0

#define kLabelFontSize 24

@implementation PJTabBackImageView

-(CGFloat) lineHalfWidth{
    return kHalfLineWidth;
}

-(CGFloat) lineButtom{
    return kLineButtom;
}

+(UIImage*) backIamgeViewType:(PJTabBackImageType) imageType{
    NSString* imageName;
    switch (imageType) {
        case PJTabBackImageTypeHalf:
            imageName = kHalfBackGoundImageName;
            break;
        case PJTabBackImageTypeLine:
            imageName = kLineBackGoundImageName;
            break;
        case PJTabBackImageTypeWide:
            imageName = kWideBackGoundImageName;
            break;
        default:
            break;
    }
    return [UIImage imageNamed:imageName];
}

-(CGFloat) backImageTabBarHeight{
    return kTabBarHeight;
}

-(id) initWithOrigin:(CGPoint) origin
           superView:(UIView*) view
           imageType:(PJTabBackImageType) imageType
               title:(NSString*) title
{
    UIImage* backGroundImage = [PJTabBackImageView backIamgeViewType:imageType];
    CGFloat selfWidth = backGroundImage.size.width;
    CGFloat selfHeight = backGroundImage.size.height;
    CGRect frame = CGRectMake(origin.x, origin.y, selfWidth, selfHeight);
    self = [super initWithFrame:frame superView:view];
    if (self) {
        // Initialization code
        [PJViewUtils generateImageViewWithImage:backGroundImage
                                          frame:CGRectMake(0, 0, selfWidth, selfHeight)
                                      superView:self
                                    contentMode:UIViewContentModeScaleToFill];
        [PJViewUtils generateLabelWithFrame:CGRectMake(0, 0, selfWidth,[self backImageTabBarHeight])
                                  superView:self
                                       text:title
                                  textColor:[UIColor whiteColor]
                              textAlignment:NSTextAlignmentCenter
                                       font:[UIFont boldSystemFontOfSize:kLabelFontSize]
                              linebreakMode:NSLineBreakByTruncatingTail
                              numberOfLines:1];
    }
    return self;
}

@end
