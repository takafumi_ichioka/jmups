//
//  PJTabHalfView.h
//  JMups
//
//  Created by flight on 2014/01/30.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"

typedef enum{
  PJTabBackImageTypeHalf = 1,
    PJTabBackImageTypeLine,
    PJTabBackImageTypeWide
} PJTabBackImageType;

@interface PJTabBackImageView : PJBaseCustomView

@property(nonatomic,readonly) CGFloat lineHalfWidth;
@property(nonatomic,readonly) CGFloat lineButtom;
@property(nonatomic,readonly) CGFloat backImageTabBarHeight;

+(UIImage*) backIamgeViewType:(PJTabBackImageType) imageType;

-(id) initWithOrigin:(CGPoint) origin
           superView:(UIView*) view
           imageType:(PJTabBackImageType) imageType
               title:(NSString*) title;

@end
