//
//  PJBaseCustomView.h
//  JMups
//
//  Created by flight on 2014/01/28.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJViewUtils.h"

@interface PJBaseCustomView : UIView

-(id) initWithFrame:(CGRect)frame superView:(UIView*) view;

@end
