//
//  PJBaseCustomView.m
//  JMups
//
//  Created by flight on 2014/01/28.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJBaseCustomView.h"

@implementation PJBaseCustomView

-(id) initWithFrame:(CGRect)frame superView:(UIView*) view{
    self = [super initWithFrame:frame];
    if (self) {
        [PJViewUtils superView:view addSubView:self];
    }
    return self;
}

@end
