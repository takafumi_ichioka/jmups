//
//  UIImage+JMups.h
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (JMups)

+(UIImage*) circleImageSize:(CGSize) size circleColor:(UIColor*) color;

@end
