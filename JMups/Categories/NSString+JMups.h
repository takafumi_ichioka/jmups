//
//  NSString+JMups.h
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JMups)

+(BOOL) isNullOrEmptyString:(NSString*) str;
+(NSString*) localizedStringForKey:(NSString*) key;
-(NSString*) whiteScapeAndNewLineTrimming;

@end
