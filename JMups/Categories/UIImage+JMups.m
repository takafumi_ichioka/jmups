//
//  UIImage+JMups.m
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "UIImage+JMups.h"



@implementation UIImage (JMups)

+(UIImage*) circleImageSize:(CGSize) size circleColor:(UIColor*) color{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 size.width,
                                                 size.height,
                                                 8,
                                                 4 * size.width,
                                                 colorSpace,
                                                 (CGBitmapInfo)kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(colorSpace);
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextSetAllowsAntialiasing(context, YES);
    CGContextFillEllipseInRect(context, CGRectMake(0, 0, size.width, size.height));
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    UIImage* circleImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGContextRelease(context);
    return circleImage;
}


@end
