//
//  NSString+JMups.m
//  JMups
//
//  Created by flight on 2014/01/29.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "NSString+JMups.h"

@implementation NSString (JMups)

+(BOOL) isNullOrEmptyString:(NSString*) str{
    return (!str || str.length == 0);
}

+(NSString*) localizedStringForKey:(NSString*) key{
    return NSLocalizedString(key, @"");
}

-(NSString*) whiteScapeAndNewLineTrimming{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
