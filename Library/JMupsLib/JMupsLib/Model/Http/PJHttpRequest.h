//
//  HttpRequest.h
//  jmapsSample
//
//  Created by 市岡 卓史 on 2014/01/10.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJURLUtils.h"
#import "PJHttpResponseUtils.h"

typedef void(^PJHttpRequestCompletedHandler)(NSData* data,NSError* error);
typedef void(^PJHttpRequestUpdateProgressHandler)(float progress);

@interface PJHttpRequest : NSOperation<NSURLConnectionDataDelegate,NSURLConnectionDelegate>

@property(nonatomic) BOOL isCertificate;
@property(nonatomic,copy) PJHttpRequestUpdateProgressHandler progressHandler;
@property(nonatomic,copy) PJHttpRequestCompletedHandler  completionHandler;

-(void) startAsyncRequestWithURL:(NSURL*) URL andParams:(NSDictionary*) params;
-(void) startAsyncRequestWithURL:(NSURL*) URL andParams:(NSDictionary*) params uniqueId:(NSString*) uniqueId;

@end
