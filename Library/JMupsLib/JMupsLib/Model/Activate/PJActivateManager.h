//
//  ActivateManager.h
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJActivateInformation.h"

@protocol PJActivateManagerDelegate;

typedef void(^PJActivateProgressHandler)(float progress);

@interface PJActivateManager : NSObject

+(PJActivateManager*) sharedManager;

-(void) startActivateInformation:(PJActivateInformation*) activateInformation
                    withDelegate:(id<PJActivateManagerDelegate>) delegate
                 progressHandler:(PJActivateProgressHandler) progessHandler;

@end

@protocol PJActivateManagerDelegate <NSObject>

@required

-(void) completedActivate:(PJActivateManager*) manager;
-(void) activateManager:(PJActivateManager*) manager failedErrorMessage:(NSString*) errorMessage;

@end

