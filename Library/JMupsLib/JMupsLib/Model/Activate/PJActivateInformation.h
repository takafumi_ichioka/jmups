//
//  ActivateInformation.h
//  jmapsSample
//
//  Created by flight on 2014/01/17.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PJActivateInformation : NSObject<NSCoding>

@property(nonatomic,copy) NSString* subscribeSequence;
@property(nonatomic,copy) NSString* activateId;
@property(nonatomic,copy) NSString* oneTimePassword;

@end
