//
//  ActivateInformation.m
//  jmapsSample
//
//  Created by flight on 2014/01/17.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "PJActivateInformation.h"

#define kSubscribeSequenceKeyName   @"subscribeSequence"
#define kActivateIdKeyName          @"activateId"
#define kOneTimePasswordKeyName     @"oneTimePassword"

@implementation PJActivateInformation


-(id) init{
    self = [super init];
    if (self) {
        _subscribeSequence = @"";
        _activateId = @"";
        _oneTimePassword = @"";
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        _subscribeSequence = [aDecoder decodeObjectForKey:kSubscribeSequenceKeyName];
        _activateId = [aDecoder decodeObjectForKey:kActivateIdKeyName];
        _oneTimePassword = [aDecoder decodeObjectForKey:kOneTimePasswordKeyName];
    }
    return self;
}

-(void) encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_subscribeSequence forKey:kSubscribeSequenceKeyName];
    [aCoder encodeObject:_activateId forKey:kActivateIdKeyName];
    [aCoder encodeObject:_oneTimePassword forKey:kOneTimePasswordKeyName];
}

-(void) dealloc{
    _subscribeSequence = nil;
    _activateId = nil;
    _oneTimePassword = nil;
}

@end
