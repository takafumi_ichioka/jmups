//
//  ActivateManager.m
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJActivateManager.h"
#import "PJHttpRequest.h"
#import "PJActivateInformation.h"
#import "PJKeyChainManager.h"
#import "PJStringUtils.h"

static PJActivateManager* _sharedInstance;

#define kInformationProgressValue    30.0f
#define kDownloadProgressValue       60.0f
#define kSendResultProgressValue     10.0f

typedef enum{
    ActivateRequstModeInformation = 1,
    ActivateRequstModeDownLoad,
    ActivateRequstModeSendResult
} ActivateRequstMode;

@interface PJActivateManager ()

@property(nonatomic,copy) PJActivateProgressHandler progressHander;
@property(nonatomic,assign) id<PJActivateManagerDelegate> delegate;
@property(nonatomic,strong) PJKeyChainInformation* keyChainInformation;

@end

@implementation PJActivateManager

+(PJActivateManager*) sharedManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[PJActivateManager alloc] init];
    });
    return _sharedInstance;
}

-(id) init{
    self = [super init];
    if (self) {
        _keyChainInformation = [PJKeyChainManager sharedManager].information;
    }
    return self;
}

-(void) startActivateInformation:(PJActivateInformation*) activateInformation
                    withDelegate:(id<PJActivateManagerDelegate>) delegate
                 progressHandler:(PJActivateProgressHandler) progessHandler{
    _keyChainInformation.activateInformation = activateInformation;
    _progressHander = progessHandler;
    _delegate = delegate;
    [self requestMode:ActivateRequstModeInformation];
    
}

-(void) requestMode:(ActivateRequstMode) requestMode{
    PJHttpRequest* httpRequest = [[PJHttpRequest alloc] init];
    __block PJActivateManager* blockSelf = self;
    httpRequest.progressHandler = ^(float progress){
        if (blockSelf.progressHander) {
            float activateProgress = 0;
            if (requestMode == ActivateRequstModeInformation) {
                activateProgress = progress / kInformationProgressValue;
            }
            else if (requestMode == ActivateRequstModeDownLoad){
                activateProgress = kInformationProgressValue + (progress/kDownloadProgressValue);
            }
            else if (requestMode == ActivateRequstModeSendResult){
                activateProgress = kInformationProgressValue + kDownloadProgressValue + (progress/kSendResultProgressValue);
            }
            blockSelf.progressHander(activateProgress);
        }
    };
    httpRequest.completionHandler = ^(NSData* data,NSError* error){
        if (error) {
            [blockSelf faildErrorDelegateMessage:error.localizedDescription];
        } else if(data){
            PJErrorInformation* errorInfo;
            if (requestMode == ActivateRequstModeDownLoad){
                errorInfo = [PJHttpResponseUtils errorInfomationByData:data];
                if (errorInfo) {
                    [blockSelf faildErrorDelegateMessage:errorInfo.errorMessage];
                } else {
                    _keyChainInformation.certificationData = [NSData dataWithData:data];
                    [blockSelf completedActivateDelegate];
                    //[blockSelf requestMode:ActivateRequstModeSendResult];
                }
                
            } else {
                BOOL successfull;
                NSDictionary* result = [PJHttpResponseUtils responseResultJsonData:data
                                                                     isSuccessfull:&successfull
                                                                         errorInfo:&errorInfo];
                if (!successfull) {
                    [blockSelf faildErrorDelegateMessage:errorInfo.errorMessage];
                } else {
                    if (requestMode == ActivateRequstModeInformation) {
                        NSString* uniqueId = [PJHttpResponseUtils uniqueIdByResponsedResult:result];
                        if ([PJStringUtils isNullOrEmptyString:uniqueId]) {
                            [blockSelf faildErrorDelegateMessage:NSLocalizedString(@"NoErrorData", @"No Data")];
                        } else {
                            [blockSelf requestMode:ActivateRequstModeDownLoad];
                        }
                    }
                    else if (requestMode == ActivateRequstModeSendResult){
                        [blockSelf completedActivateDelegate];
                    }
                }
            }
        } else {
            [blockSelf faildErrorDelegateMessage:NSLocalizedString(@"NoErrorData", @"No Data")];
        }
        
    };
    NSURL* URL;
    NSDictionary* params = @{};
    NSString* uniqueId;
    if (requestMode == ActivateRequstModeInformation) {
        PJLog(@"%@",_keyChainInformation.activateInformation.subscribeSequence);
        PJLog(@"%@",_keyChainInformation.activateInformation.activateId);
        PJLog(@"%@",_keyChainInformation.activateInformation.oneTimePassword);
        params = @{@"subscribeSequence":_keyChainInformation.activateInformation.subscribeSequence,
                   @"activateID":_keyChainInformation.activateInformation.activateId,
                   @"oneTimePassword":_keyChainInformation.activateInformation.oneTimePassword};
        URL = [PJURLUtils activateURL];
    }
    else if (requestMode == ActivateRequstModeDownLoad){
        params = @{@"subscribeSequence":_keyChainInformation.activateInformation.subscribeSequence,
                   @"activateID":_keyChainInformation.activateInformation.activateId,
                   @"oneTimePassword":_keyChainInformation.activateInformation.oneTimePassword,
                   @"termPrimaryNo":_keyChainInformation.termPrimaryNo};
        URL = [PJURLUtils downloadCertificationURL];
    }
    else if (requestMode == ActivateRequstModeSendResult){
        params = @{@"uniqueId":_keyChainInformation.uniqueId};
        URL = [PJURLUtils sendResultActivateURL];
        uniqueId = _keyChainInformation.uniqueId.copy;
        return;
    }
    [httpRequest startAsyncRequestWithURL:URL andParams:params uniqueId:uniqueId];
}

-(void) faildErrorDelegateMessage:(NSString*) message{
    if (_delegate && [_delegate respondsToSelector:@selector(activateManager:failedErrorMessage:)]) {
        [_delegate activateManager:self failedErrorMessage:message];
    }
}

-(void) completedActivateDelegate{
    if (_delegate && [_delegate respondsToSelector:@selector(completedActivate:)]) {
        [_delegate completedActivate:self];
    }
}

@end
