//
//  URLUtils.m
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJURLUtils.h"
#import "PJStringUtils.h"

static NSDictionary* _savedURLInfo;

@implementation PJURLUtils

+(void) setURLInfo:(NSDictionary*) URLInfo{
    _savedURLInfo = URLInfo.copy;
}

+(NSURL*) activateURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"ActivateURL"];
}

+(NSURL*) downloadCertificationURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"DownloadCertificationURL"];
}

+(NSURL*) sendResultActivateURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"SendResultActivateURL"];
}
+(NSURL*) openURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"OpenURL"];
}

+(NSURL*) closeURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"CloseURL"];
}

+(NSURL*) stopServiceURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"StopServiceURL"];
}

+(NSURL*) creditDayInformationURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"CreditDayInformationURL"];
}

+(NSURL*) creditDayResultURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"CreditDayResultURL"];
}

+(NSURL*) creditJournalURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"CreditJournalURL"];
}

+(NSURL*) creditReprintURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"CreditReprintURL"];
}

+(NSURL*) cardCompanryListURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"CardCompanryListURL"];
}

+(NSURL*) informationListURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"InformationListURL"];
}

+(NSURL*) downloadLogoImageURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"DownloadLogoImage"];
}

+(NSURL*) termServiceSettingsURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"TermServiceSettingsURL"];
}
+(NSURL*) creditTradeStartURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"CreditTradeStart"];
}

+(NSURL*) creditReadingURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"CreditReadingURL"];
}

+(NSURL*) initICCApplicationURL{
    return [self targetURLRequiredCertificate:NO requestIDURLKey:@"InitICCApplicationURL"];
}


+(NSURL*) targetURLRequiredCertificate:(BOOL) certficate
                          requestIDURLKey:(NSString*) requestIdURLkey{
    NSDictionary* URLInfo = _savedURLInfo.copy;
    NSString* url = [NSString stringWithFormat:@"%@:%@/%@/%@",URLInfo[@"BaseURL"],
                                                              URLInfo[certficate ? @"CerrificationPortNumber":@"NotCertificationPortNumber"],
                                                              URLInfo[@"BaseURLComponent"],
                                                              URLInfo[@"RequestIDURLS"][requestIdURLkey]];
    return [NSURL URLWithString:url];
}

+(NSURLRequest*) genarateAPIRequestWithURL:(NSURL*) URL
                                 andParams:(NSDictionary*) params
                                  uniqueId:(NSString*) uniqueId{
    PJLog(@"URL = %@",URL);
    PJLog(@"params = %@",params);
    if (!params) {
        params = @{};
    }
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSLog(@"jsonData = %@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:30];
    [request setHTTPBody:jsonData];
    if (uniqueId) {
        PJLog(@"uniqueId = %@",uniqueId);
        [request setValue:uniqueId forHTTPHeaderField:@"uniqueId"];
    }
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    return request;
}

@end
