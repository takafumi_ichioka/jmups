//
//  URLUtils.h
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PJURLUtils : NSObject

+(void) setURLInfo:(NSDictionary*) URLInfo;
+(NSURL*) activateURL;
+(NSURL*) downloadCertificationURL;
+(NSURL*) sendResultActivateURL;
+(NSURL*) openURL;
+(NSURL*) closeURL;
+(NSURL*) stopServiceURL;
+(NSURL*) creditDayInformationURL;
+(NSURL*) creditDayResultURL;
+(NSURL*) creditJournalURL;
+(NSURL*) creditReprintURL;
+(NSURL*) cardCompanryListURL;
+(NSURL*) informationListURL;
+(NSURL*) downloadLogoImageURL;
+(NSURL*) termServiceSettingsURL;
+(NSURL*) creditTradeStartURL;
+(NSURL*) creditReadingURL;
+(NSURL*) initICCApplicationURL;
+(NSURLRequest*) genarateAPIRequestWithURL:(NSURL*) URL
                                 andParams:(NSDictionary*) params
                                  uniqueId:(NSString*) uniqueId;

@end
