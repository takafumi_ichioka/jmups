//
//  StringUtils.h
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PJStringUtils : NSObject

+(BOOL) isNullOrEmptyString:(NSString*) str;
+(BOOL) equalTrueToString:(NSString*) str;
+(NSString*) UUIDString;

@end
