//
//  StringUtils.m
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJStringUtils.h"

@implementation PJStringUtils

+(BOOL) isNullOrEmptyString:(NSString*) str{
    return (!str || str.length == 0);
}
+(BOOL) equalTrueToString:(NSString*) str{
    return [[str lowercaseString] isEqualToString:@"true"];
}

+(NSString*) UUIDString{
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
	NSString *uuidStr = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
	CFRelease(uuid);
	return uuidStr;
}

@end
