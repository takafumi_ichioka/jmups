//
//  JMUPUtils.m
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJHttpResponseUtils.h"
#import "PJStringUtils.h"

@implementation PJHttpResponseUtils

+(NSDictionary*) responseResultJsonData:(NSData*) jsonData
                          isSuccessfull:(BOOL*) success
                              errorInfo:(PJErrorInformation**) errorInfo{
    if (!jsonData) {
        if (success) {
            *success = NO;
        }
        return nil;
    }
    NSError* error;
    NSDictionary* result = [NSJSONSerialization JSONObjectWithData:jsonData
                                                           options:NSJSONReadingAllowFragments
                                                             error:&error];
    if (error) {
        *success = NO;
        return nil;
    }
    id resultCode = result[@"resultCode"];
    BOOL ok = (resultCode && [resultCode integerValue] == 0);
    *success = ok;
    if (!ok) {
       *errorInfo = [self errorInformationByErrorObj:result[@"errorObject"]];
    }
    return result;
}

+(PJErrorInformation*) errorInfomationByData:(NSData*) data{
    if (!data) {
        return nil;
    }
    NSError* error;
    NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data
                                                           options:NSJSONReadingAllowFragments
                                                             error:&error];
    if (error || !result) {
        return nil;
    }
    return [self errorInformationByErrorObj:result[@"errorObject"]];
}

+(NSString*) uniqueIdByResponsedResult:(NSDictionary*) result{
    return result[@"normalObject"][@"unique_id"];
}

+(PJErrorInformation*) errorInformationByErrorObj:(NSDictionary*) errorObj{
    if (!errorObj) {
        return nil;
    }
    PJErrorInformation* errorInfo = [[PJErrorInformation alloc] init];
    errorInfo.errorCode = errorObj[@"errorCode"];
    errorInfo.errorMessage = errorObj[@"errorMessage"];
    errorInfo.printBugReport = [PJStringUtils equalTrueToString:errorObj[@"printBugReport"]];
    errorInfo.errorType = errorObj[@"errorType"];
    errorInfo.errorLevel = errorObj[@"errorLevel"];
    return errorInfo;
}

@end
