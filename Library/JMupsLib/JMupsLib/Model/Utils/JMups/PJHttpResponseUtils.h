//
//  JMUPUtils.h
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJErrorInformation.h"

@interface PJHttpResponseUtils : NSObject

+(NSDictionary*) responseResultJsonData:(NSData*) jsonData
                          isSuccessfull:(BOOL*) success
                              errorInfo:(PJErrorInformation**) errorInfo;

+(PJErrorInformation*) errorInfomationByData:(NSData*) data;

+(NSString*) uniqueIdByResponsedResult:(NSDictionary*) result;

@end
