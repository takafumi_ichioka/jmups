//
//  EMVDataUtils.m
//  jmapsSample
//
//  Created by flight on 2014/01/24.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "PJEMVDataUtils.h"

@implementation PJEMVDataUtils

+(NSData*) endCommandValue{
    return [NSData dataWithBytes:"\x00" length:1];
}
+(NSData*) selectCommand{
    return [NSData dataWithBytes:"\x00\xa4" length:2];
}
+(NSData*) readRecordCommand{
    return [NSData dataWithBytes:"\x00\xb2" length:2];
}
+(NSArray*) cardCompanyAIDList{
    return @[[NSData dataWithBytes:"\xa0\x00\x00\x00\x65\x10x10" length:7],
             [NSData dataWithBytes:"\xa0\x00\x00\x00\x03\x10x10" length:7],
             [NSData dataWithBytes:"\xa0\x00\x00\x00\x04\x10x10" length:7],
             [NSData dataWithBytes:"\xa0\x00\x00\x00\x25x01" length:6],
             [NSData dataWithBytes:"\xa0\x00\x00\x01\x52\x30x10" length:7]];
}
+(NSData*) selectParam{
    return [NSData dataWithBytes:"\x04\x00" length:2];
}
+(NSData*) FCITempleteTag{
    return [NSData dataWithBytes:"\x70" length:1];
}
+(NSData*) dFNameTag{
    return [NSData dataWithBytes:"\x84" length:1];
}
+(NSData*) FCIPropertyTempleteTag{
    return [NSData dataWithBytes:"\xa5" length:1];
}
+(NSData*) ADFNameTag{
    return [NSData dataWithBytes:"\x4f" length:1];
}
+(NSData*) applicationLabelTag{
    return [NSData dataWithBytes:"\x50" length:1];
}
+(NSData*) applicationPriorityIndicatorTag{
    return [NSData dataWithBytes:"\x87" length:1];
}
+(NSData*) directoryDiscretionaryTempleteTag{
    return [NSData dataWithBytes:"\x73" length:1];
}
+(NSData*) EMVProprietaryTemplateTag{
    return [NSData dataWithBytes:"\x70" length:1];
}

+(NSString*) stringValueWithData:(NSData*) data{
    NSString* stringValue = data.description;
    stringValue = [stringValue stringByReplacingOccurrencesOfString:@"<" withString:@""];
    stringValue = [stringValue stringByReplacingOccurrencesOfString:@">" withString:@""];
    stringValue = [stringValue stringByReplacingOccurrencesOfString:@" " withString:@""];
    return stringValue;
}

+(NSData*) createEmvCommandData:(NSData*) commandData
                      paramData:(NSData*) paramData
                    optinalData:(NSData*) optionalData
                        endData:(NSData *)endData{
    if (!commandData || !paramData) {
        return nil;
    }
    NSMutableData *emvData = commandData.mutableCopy;
    [emvData appendData:paramData];
    NSData* addingOptionalData = [self addingLengthData:optionalData];
    if (addingOptionalData) {
        [emvData appendData:addingOptionalData];
    }
    if (endData && endData.length > 0) {
        [emvData appendData:endData];
    }
    return emvData;
}

+(NSData*) searchTagData:(NSData *)tagData withData:(NSData *)data resultData:(NSData **)resultData{
    if (!data || !tagData || !resultData) {
        *resultData = nil;
        return data;
    }
    NSUInteger dataLength = data.length;
    if (tagData.length > dataLength) {
        *resultData = nil;
        return data;
    }
    NSRange dataRange = [data rangeOfData:tagData
                                  options:NSDataSearchAnchored
                                    range:NSMakeRange(0, dataLength - tagData.length - 1)];
    if (dataRange.location == NSNotFound) {
        *resultData = nil;
        return data;
    }
    NSUInteger searchDataLoc = dataRange.location + dataRange.length;
    NSUInteger searchDataLength = dataLength - searchDataLoc;
    if (searchDataLength == 0) {
        *resultData = nil;
        return data;
    }
    NSData* searchData = [data subdataWithRange:NSMakeRange(searchDataLoc, searchDataLength)];
    NSData* lengthData = [searchData subdataWithRange:NSMakeRange(0, 1)];
    unsigned int len = [self lengthValueWithData:lengthData];
    *resultData = [searchData subdataWithRange:NSMakeRange(1, len)];
    NSUInteger dataLoc = searchDataLoc + 1 + len;
    NSUInteger savedDataLength = dataLength;
    dataLength = dataLength - dataLoc;
    if (dataLength > 0) {
        data = [data subdataWithRange:NSMakeRange(dataLoc, dataLength)];
    } else {
        dataLoc = searchDataLoc + 1;
        dataLength = savedDataLength - dataLoc;
        data = [data subdataWithRange:NSMakeRange(dataLoc, dataLength)];
    }
    return data;
}

+(unsigned int) lengthValueWithData:(NSData*) data{
    NSString* stringValue = [self stringValueWithData:data];
    unsigned int valueLength;
    [[NSScanner scannerWithString:stringValue] scanHexInt:&valueLength];
    return valueLength;
}

+(NSData*) addingLengthData:(NSData*) data{
    if (!data || data.length == 0) {
        return nil;
    }
    NSMutableData* addingLengthData = [NSMutableData data];
    int dataLength = (int)data.length;
    [addingLengthData appendBytes:&dataLength length:1];
    [addingLengthData appendData:data];
    return addingLengthData;
}

@end
