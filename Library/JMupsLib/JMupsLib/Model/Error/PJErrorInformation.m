//
//  ErrorInformation.m
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJErrorInformation.h"

@implementation PJErrorInformation

-(id) init{
    self = [super init];
    if (self) {
        _errorCode = @"";
        _errorMessage = @"";
        _printBugReport = NO;
        _errorType = @"";
        _errorLevel = @"";
    }
    return self;
}

-(void) dealloc{
    _errorCode = nil;
    _errorMessage = nil;
    _errorType = nil;
    _errorLevel = nil;
}

@end
