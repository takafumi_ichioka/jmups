//
//  ErrorInformation.h
//  JMupsLib
//
//  Created by flight on 2014/01/27.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PJErrorInformation : NSObject

@property(nonatomic,copy) NSString* errorCode;
@property(nonatomic,copy) NSString* errorMessage;
@property(nonatomic)      BOOL      printBugReport;
@property(nonatomic,copy) NSString* errorType;
@property(nonatomic,copy) NSString* errorLevel;

@end
