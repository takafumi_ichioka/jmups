//
//  KeyChainInformation.h
//  jmapsSample
//
//  Created by flight on 2014/01/17.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJActivateInformation.h"

@interface PJKeyChainInformation : NSObject<NSCoding>

@property(nonatomic,strong) NSString* termPrimaryNo;
@property(nonatomic,strong) NSString* uniqueId;
@property(nonatomic,strong) NSData*   certificationData;
@property(nonatomic,strong) PJActivateInformation* activateInformation;

@end
