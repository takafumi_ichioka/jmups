//
//  KeyChainManager.h
//  jmapsSample
//
//  Created by flight on 2014/01/17.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJKeyChainInformation.h"

@interface PJKeyChainManager : NSObject

+(PJKeyChainManager*) sharedManager;

@property(nonatomic,strong) PJKeyChainInformation* information;

@end
