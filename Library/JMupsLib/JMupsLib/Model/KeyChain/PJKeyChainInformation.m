//
//  KeyChainInformation.m
//  jmapsSample
//
//  Created by flight on 2014/01/17.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "PJKeyChainInformation.h"

#define kUnidueIdKeyName                @"uniqueId"
#define kTermPrimaryNoKeyName           @"termPrimaryNo"
#define kCertificationDataKeyName       @"certificationData"
#define kActivateInformationKeyName     @"activateInformation"


@implementation PJKeyChainInformation

-(id) initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        _uniqueId = [aDecoder decodeObjectForKey:kUnidueIdKeyName];
        _termPrimaryNo = [aDecoder decodeObjectForKey:kTermPrimaryNoKeyName];
        _certificationData = [aDecoder decodeObjectForKey:kCertificationDataKeyName];
        NSData* activateData = [aDecoder decodeObjectForKey:kActivateInformationKeyName];
        if (activateData) {
           _activateInformation = [NSKeyedUnarchiver unarchiveObjectWithData:activateData];
        }
        
    }
    return self;
}

-(void) encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_uniqueId forKey:kUnidueIdKeyName];
    [aCoder encodeObject:_termPrimaryNo forKey:kTermPrimaryNoKeyName];
    [aCoder encodeObject:_certificationData forKey:kCertificationDataKeyName];
    if (_activateInformation) {
        NSData* activateData = [NSKeyedArchiver archivedDataWithRootObject:_activateInformation];
        if (activateData) {
            [aCoder encodeObject:activateData forKey:kActivateInformationKeyName];
        }
    }
    
}

-(void) dealloc{
    _uniqueId = nil;
    _termPrimaryNo = nil;
    _certificationData = nil;
    _activateInformation = nil;
}

@end
