//
//  KeyChainManager.m
//  jmapsSample
//
//  Created by flight on 2014/01/17.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "PJKeyChainManager.h"
#import "PJStringUtils.h"

#define kSavedInformationKeyChainKeyName @"savedInformationKeyChainKeyName"
#define kSavedInformationUserDefaultKeyName @"savedInformationUserDefaultKeyName"

static PJKeyChainManager* _sharedInstance;

@implementation PJKeyChainManager

@synthesize information = _information;

+(PJKeyChainManager*) sharedManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[PJKeyChainManager alloc] init];
    });
    return _sharedInstance;
}

-(void) setInformation:(PJKeyChainInformation *)information{
    _information = information;
    [self savedKeyChainInformation:information];
}

-(PJKeyChainInformation*) information{
    if (!_information) {
        PJKeyChainInformation* information = [self loadedKeyChainInformation];
        if (!information) {
            information = [[PJKeyChainInformation alloc] init];
            NSString* uuidString = [PJStringUtils UUIDString];
            uuidString = [uuidString stringByReplacingOccurrencesOfString:@"-" withString:@""];
            information.termPrimaryNo = uuidString;
            [self savedKeyChainInformation:information];
        }
        _information = information;
    }
    return _information;
}

-(void) savedKeyChainInformation:(PJKeyChainInformation*) information{
    if (!information) {
        return;
    }
    NSDictionary* keyChainQuery = [self keyChainQuery];
    NSData* newData = [NSKeyedArchiver archivedDataWithRootObject:information];
    NSData* savedData = [self savedKeyChainDataAtKeyChainQuery:keyChainQuery];
    NSMutableDictionary* newItem = @{}.mutableCopy;
    newItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    newItem[(__bridge id)kSecAttrService] = kSavedInformationKeyChainKeyName;
    newItem[(__bridge id)kSecValueData] = newData;
    if (savedData) {
        SecItemUpdate((__bridge CFDictionaryRef)keyChainQuery,(__bridge CFDictionaryRef)newItem);
    } else {
        SecItemAdd((__bridge CFDictionaryRef)newItem, nil);
    }
    NSUserDefaults* userDefaluts = [NSUserDefaults standardUserDefaults];
    [userDefaluts setObject:newData forKey:kSavedInformationUserDefaultKeyName];
    [userDefaluts synchronize];
}

-(PJKeyChainInformation*) loadedKeyChainInformation{
    NSData* data = nil;
    NSDictionary* keyChainQuery = [self keyChainQuery];
    data = [self savedKeyChainDataAtKeyChainQuery:keyChainQuery];
    if (!data) {
        data = [[NSUserDefaults standardUserDefaults] objectForKey:kSavedInformationUserDefaultKeyName];
        if (!data) {
            return nil;
        }
    }
    PJKeyChainInformation* information = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return information;
}

-(NSDictionary*) keyChainQuery{
    NSMutableDictionary* keyChainQuery = @{}.mutableCopy;
    keyChainQuery[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keyChainQuery[(__bridge id)kSecAttrService] = kSavedInformationKeyChainKeyName;
    keyChainQuery[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;
    keyChainQuery[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    return keyChainQuery;
}

-(NSData*) savedKeyChainDataAtKeyChainQuery:(NSDictionary*) keyChainQuery{
    NSData* data = nil;
    CFTypeRef ref;
    OSStatus res = SecItemCopyMatching((__bridge CFDictionaryRef)keyChainQuery, &ref);
    if (res == noErr) {
        NSMutableDictionary* readItem = ((__bridge NSDictionary*)ref).mutableCopy;
        readItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
        readItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
        CFTypeRef pass;
        res = SecItemCopyMatching((__bridge CFDictionaryRef)readItem, &pass);
        if (res == noErr) {
            data = (__bridge NSData*)pass;
        }
        return nil;
    }
    return data;
}

-(NSString*) termPrimaryNo{
    NSMutableDictionary* keyChainQuery = @{}.mutableCopy;
    keyChainQuery[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keyChainQuery[(__bridge id)kSecAttrService] = @"UUIDTest";
    keyChainQuery[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;
    keyChainQuery[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    
    CFTypeRef ref;
    OSStatus res = SecItemCopyMatching((__bridge CFDictionaryRef)keyChainQuery, &ref);
    NSString* termPrimaryNo = @"";
    if (res == noErr) {
        NSMutableDictionary* readItem = ((__bridge NSDictionary*)ref).mutableCopy;
        readItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
        readItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
        CFTypeRef pass;
        res = SecItemCopyMatching((__bridge CFDictionaryRef)readItem, &pass);
        if (res == noErr) {
            NSData* data = (__bridge NSData*)pass;
            termPrimaryNo = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
    } else {
        termPrimaryNo = [PJStringUtils UUIDString];
        termPrimaryNo = [termPrimaryNo stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSMutableDictionary* addItem = @{}.mutableCopy;
        addItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
        addItem[(__bridge id)kSecAttrService] = @"UUIDTest";
        addItem[(__bridge id)kSecValueData] = [termPrimaryNo dataUsingEncoding:NSUTF8StringEncoding];
        
        SecItemAdd((__bridge CFDictionaryRef)addItem, nil);
    }
    return termPrimaryNo;
}

-(NSData*) keyChainDataForKey:(NSString*) key{
    if (!key) {
        return nil;
    }
    NSData* data = nil;
    NSMutableDictionary* keyChainQuery = @{}.mutableCopy;
    keyChainQuery[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keyChainQuery[(__bridge id)kSecAttrService] = key;
    keyChainQuery[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;
    keyChainQuery[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    
    CFTypeRef ref;
    OSStatus res = SecItemCopyMatching((__bridge CFDictionaryRef)keyChainQuery, &ref);
    if (res == noErr) {
        NSMutableDictionary* readItem = ((__bridge NSDictionary*)ref).mutableCopy;
        readItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
        readItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
        CFTypeRef pass;
        res = SecItemCopyMatching((__bridge CFDictionaryRef)readItem, &pass);
        if (res == noErr) {
            data = (__bridge NSData*)pass;
        }
    }
    return data;
}

@end
