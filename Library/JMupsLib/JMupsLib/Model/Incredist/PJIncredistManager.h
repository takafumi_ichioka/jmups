//
//  IncredistManager.h
//  jmapsSample
//
//  Created by flight on 2014/01/24.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FSIncredistLib.h"
#import "PJEMVDataUtils.h"

typedef void(^PJIncredistCompletedResponseHandler)(FSIncredistLib* deveice,BOOL connect);
typedef void(^PJIncredistCompletedEMVCommandHandler)(NSData* metaData,NSData* sw1Data,NSData* sw2Data);
typedef void(^PJIncredistErrorHandler)(NSString* errorHandler);

@interface PJIncredistManager : NSObject<FSIncredistLibDelegate>

+(PJIncredistManager*) sharedManager;

@property(nonatomic,copy) PJIncredistCompletedResponseHandler completedResponseHandler;
@property(nonatomic,copy) PJIncredistCompletedEMVCommandHandler completedEMVCommandHandler;
@property(nonatomic,copy) PJIncredistErrorHandler             responseErrorHandler;

-(void) resetAIDList;

-(void) connectDevice;
-(void) disConnectDevive;

-(void) emvOpen;
-(void) selectFileName:(NSData*) flieName;
-(BOOL) nextSelectAID;
-(void) readRecordByNumber:(int) recordNumber sfiNumber:(int) sfiNumber;
-(void) emvClose;
-(void) answerToReset;


@end
