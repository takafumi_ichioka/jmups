//
//  IncredistManager.m
//  jmapsSample
//
//  Created by flight on 2014/01/24.
//  Copyright (c) 2014年 市岡 卓史. All rights reserved.
//

#import "PJIncredistManager.h"

static PJIncredistManager* _sharedInstance;

@interface PJIncredistManager ()

@property(nonatomic,assign) FSIncredistLib* incredistDevice;
@property(nonatomic,copy) NSMutableArray*  aidList;


@end

@implementation PJIncredistManager

+(PJIncredistManager*) sharedManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[PJIncredistManager alloc] init];
    });
    return _sharedInstance;
}

-(id) init{
    self = [super init];
    if (self) {
        _incredistDevice = [FSIncredistLib sharedInstance];
        _incredistDevice.delegate = self;
        CFRunLoopRunInMode(kCFRunLoopDefaultMode, 1.0, NO);
    }
    return self;
}


-(void) connectDevice{
    NSString* blueToothAddress = _incredistDevice.readBluetoothAddressToDevice;
    if (blueToothAddress.length > 0) {
        [_incredistDevice connectToRegisteredDevices:blueToothAddress withTimeout:30];
    } else {
        [_incredistDevice connectToRegisteredDevices:nil withTimeout:30];
    }
}

-(void) disConnectDevive{
    if (_incredistDevice.connected) {
        [_incredistDevice disconnect];
    }
}

-(void) resetAIDList{
    [_aidList removeAllObjects];
    _aidList = nil;
}

-(void) emvOpen{
    [_incredistDevice emvOpen];
}
-(void) selectFileName:(NSData *)flieName{
    [self executeEmvCommand:[PJEMVDataUtils selectCommand]
                  paramData:[PJEMVDataUtils selectParam]
               optionalData:flieName
            isAppendEndData:YES
              isAdpuCommand:YES];
}

-(BOOL) nextSelectAID{
    NSData* aidData;
    if (_aidList && _aidList.count > 0) {
        aidData = _aidList[0];
        [_aidList removeObjectAtIndex:0];
    } else {
        _aidList = nil;
        _aidList = [PJEMVDataUtils cardCompanyAIDList].mutableCopy;
        if (_aidList.count > 0) {
            aidData = _aidList[0];
        }
    }
    BOOL isNext = (aidData && aidData.length > 0);
    if (isNext) {
        __weak PJIncredistManager* weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakSelf selectFileName:aidData];
        });
    }
    return isNext;
}

-(void) readRecordByNumber:(int) recordNumber sfiNumber:(int) sfiNumber{
    if (recordNumber < 0 || sfiNumber < 0) {
        return;
    }
    NSMutableData* paramData = [NSMutableData data];
    [paramData appendBytes:&recordNumber length:1];
    int sfiNumberParam = sfiNumber << 3;
    sfiNumberParam += 4;
    [paramData appendBytes:&sfiNumberParam length:1];
    
    [self executeEmvCommand:[PJEMVDataUtils readRecordCommand]
                  paramData:paramData
               optionalData:nil
            isAppendEndData:YES
              isAdpuCommand:YES];
}

-(void) answerToReset{
    [self executeEmvCommand:[NSData dataWithBytes:"\x80\xca" length:2]
                  paramData:[NSData dataWithBytes:"\x9f\x17" length:2]
               optionalData:nil
            isAppendEndData:YES
              isAdpuCommand:NO];
}

-(void) emvClose{
    [_incredistDevice emvClose];
}

-(void) executeEmvCommand:(NSData*) commandData
                paramData:(NSData*) paramData
             optionalData:(NSData*) optionalData
          isAppendEndData:(BOOL) isAppendEnd
            isAdpuCommand:(BOOL) isAdpuCommand{
    NSData* endData = (isAppendEnd) ? [PJEMVDataUtils endCommandValue] : nil;
    NSData* emvCommandData = [PJEMVDataUtils createEmvCommandData:commandData
                                                      paramData:paramData
                                                    optinalData:optionalData
                                                        endData:endData];
    if (isAdpuCommand) {
        [_incredistDevice emvSendApduCommand:emvCommandData];
    } else {
        [_incredistDevice emvSendCommand:emvCommandData];
    }
    
//    if ([NSThread isMainThread]) {
//        [_incredistDevice emvSendApduCommand:emvCommandData];
//    } else {
//        __weak IncredistManager* weakSelf = self;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakSelf.incredistDevice emvSendApduCommand:emvCommandData];
//        });
//    }
    
}

-(void) executeCompletedEMVCommandHandlerWithEMVResonse:(NSData*) emvResponse{
    NSData* sw1Data;
    NSData* sw2Data;
    NSData* metaData;
    if (emvResponse) {
        NSUInteger emvResponseLength = emvResponse.length;
        if (emvResponseLength >= 2) {
            sw1Data = [emvResponse subdataWithRange:NSMakeRange(emvResponseLength - 2, 1)];
            sw2Data = [emvResponse subdataWithRange:NSMakeRange(emvResponseLength - 1, 1)];
            if (emvResponseLength > 2) {
                metaData = [emvResponse subdataWithRange:NSMakeRange(0, emvResponseLength - 2)];
            }
        }
    }
    [self completedHandlerMetaData:metaData
                           sw1Data:sw1Data
                           sw2Data:sw2Data];
}

-(void) completedHandlerMetaData:(NSData*) metaData
                         sw1Data:(NSData*) sw1Data
                         sw2Data:(NSData*) sw2Data{
    if (_completedEMVCommandHandler) {
        _completedEMVCommandHandler(metaData,sw1Data,sw2Data);
    }
}

-(void) incredistDidConnect:(FSIncredistLib *)device{
    if (device.connectionMode == FSConnectionModeDock) {
        [device registerBluetoothAddressToDevice];
    }
    if (_completedResponseHandler) {
        _completedResponseHandler(device,YES);
    }
}

-(void) incredistDidDisconnect:(FSIncredistLib *)device{
    if (_completedResponseHandler) {
        _completedResponseHandler(device,NO);
    }
}

-(void) connectError:(FSIncredistLib *)device error:(NSString *)errorMessage{
    if (_responseErrorHandler) {
        _responseErrorHandler(errorMessage);
    }
}

-(void) scannerError:(FSIncredistLib *)device error:(NSString *)errorMessage{
    if (_responseErrorHandler) {
        _responseErrorHandler(errorMessage);
    }
}

- (void)didGetEMVICCOpenResponse:(NSData *)emvResponse{
    NSString* responseStr = [PJEMVDataUtils stringValueWithData:emvResponse];
    const char* sw1;
    const char* sw2;
    if ([responseStr isEqualToString:@"00"]) {
        sw1 = "\x90";
        sw2 = "\x00";
    } else {
        sw1 = "\xFF";
        sw2 = "\xFF";
    }
    [self completedHandlerMetaData:nil
                           sw1Data:[NSData dataWithBytes:sw1 length:1]
                           sw2Data:[NSData dataWithBytes:sw2 length:1]];
}

- (void)didGetEMVICCSendCommandResponse:(NSData *)emvResponse{
    [self executeCompletedEMVCommandHandlerWithEMVResonse:emvResponse];
}
- (void)didGetEMVICCCloseResponse{
    [self completedHandlerMetaData:nil
                           sw1Data:[NSData dataWithBytes:"\x90" length:1]
                           sw2Data:[NSData dataWithBytes:"\x00" length:1]];
}

- (void)didScanICCard{
    PJLog(@"didScanICCard");
}
- (void)didInputICCardPin:(FSMagneticCardType)cardType ksn:(NSString *)ksn track:(NSArray *)track
				   cardNo:(NSString *)cardNo expirationDate:(NSString *)date{
    PJLog(@"didInputICCardPin = %d\nksn = %@\ntrack =%@\ncardNo =%@\nexpirationDate =%@",cardType,track,ksn,cardNo,date);
    
}

@end
