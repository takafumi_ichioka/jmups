//
//  PJLibInterface.h
//  JMupsLib
//
//  Created by flight on 2014/01/28.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PJActivateInformation.h"
#import "PJKeyChainInformation.h"

@protocol PJLibInterfaceDelegate;

@interface PJLibInterface : NSObject

@property(nonatomic,assign) id<PJLibInterfaceDelegate> delegate;
@property(nonatomic,strong) PJActivateInformation* activeInformation;
@property(nonatomic,strong,readonly) PJKeyChainInformation* keychainInformation;

+(PJLibInterface*) sharedInterface;
-(void) startActivate;
-(void) settingURLInfoPlistName:(NSString*) plistName;

@end

@protocol PJLibInterfaceDelegate <NSObject>

@optional
-(void) didCompletedActivate;
-(void) updateActivateProgress:(float) progress;
-(void) didFaildActivateErrorMessage:(NSString*) errorMessage;

@end
