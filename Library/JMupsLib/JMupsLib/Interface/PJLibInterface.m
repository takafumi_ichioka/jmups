//
//  PJLibInterface.m
//  JMupsLib
//
//  Created by flight on 2014/01/28.
//  Copyright (c) 2014年 flight. All rights reserved.
//

#import "PJLibInterface.h"
#import "PJActivateManager.h"
#import "PJURLUtils.h"
#import "PJKeyChainManager.h"

static PJLibInterface* _sharedInstance;

@interface PJLibInterface ()<PJActivateManagerDelegate>


@end

@implementation PJLibInterface

+(PJLibInterface*) sharedInterface{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[PJLibInterface alloc] init];
    });
    return _sharedInstance;
}

-(void) settingURLInfoPlistName:(NSString*) plistName{
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
    NSDictionary* URLInfo = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    [PJURLUtils setURLInfo:URLInfo];
}


-(void) startActivate{
    PJActivateManager* activateManager = [PJActivateManager sharedManager];
    __block PJLibInterface* blockSelf = self;
    [activateManager startActivateInformation:_activeInformation withDelegate:self progressHandler:^(float progress){
        if (blockSelf.delegate && [blockSelf.delegate respondsToSelector:@selector(updateActivateProgress:)]) {
            [blockSelf.delegate updateActivateProgress:progress];
        }
    }];
}

#pragma Activate Delegate
-(void) completedActivate:(PJActivateManager *)manager{
    if (_delegate && [_delegate respondsToSelector:@selector(didCompletedActivate)]) {
        [_delegate didCompletedActivate];
    }
}

-(void) activateManager:(PJActivateManager *)manager failedErrorMessage:(NSString *)errorMessage{
    if (_delegate && [_delegate respondsToSelector:@selector(didFaildActivateErrorMessage:)]) {
        [_delegate didFaildActivateErrorMessage:errorMessage];
    }
}

@end
